<?php

/**
 * Define routes here.
 *
 * Routes follow this format:
 *
 * [METHOD, ROUTE, CALLABLE] or
 * [METHOD, ROUTE, [Class => method]]
 *
 * When controller is used without method (as string), it needs to have a magic __invoke method defined.
 *
 * Routes can use optional segments and regular expressions. See nikic/fastroute
 */

return [
    [['GET'], '/', \Gfinance\Timelog\Action\Index::class],

    [['GET', 'POST'], '/login/{action}/', \Skeletor\Login\Controller\LoginController::class],
    [['GET', 'POST'], '/user/{action}/[{id}/]', \Skeletor\User\Controller\UserController::class],
    [['GET', 'POST'], '/tag/{action}/[{id}/]', \Skeletor\Tag\Controller\TagController::class],
    [['GET', 'POST'], '/client/{action}/[{id}/]', \Gfinance\Client\Controller\ClientController::class],
    [['GET', 'POST'], '/project/{action}/[{id}/]', \Gfinance\Project\Controller\ProjectController::class],
    [['GET', 'POST'], '/task/{action}/[{id}/]', \Gfinance\Task\Controller\TaskController::class],
    [['GET', 'POST'], '/feature/{action}/[{id}/]', \Gfinance\Project\Controller\FeatureController::class],

    [['GET', 'POST'], '/deploy/{action}/[{id}/]', \Gfinance\Project\Controller\DeployController::class],


    [['GET', 'POST'], '/comment/{action}/[{id}/]', \Gfinance\Task\Controller\CommentController::class],
    [['GET', 'POST'], '/timelog/{action}/[{id}/]', \Gfinance\Task\Controller\TimelogController::class],
    [['GET', 'POST'], '/ticketing/{action}/[{id}/]', \Gfinance\Timelog\Controller\TicketingController::class],
    [['GET', 'POST'], '/watchlist/{action}/[{id}/]', \Gfinance\Timelog\Controller\WatchlistController::class],


    [['POST'], '/api/authenticate/', \Gfinance\Api\Controller\LoginController::class],
    [['POST'], '/api/refresh/', \Gfinance\Api\Controller\RefreshController::class],
    [['GET', 'POST'], '/api/task/{action}/[{id}/]', \Gfinance\Api\Controller\TaskController::class],
    [['GET', 'POST'], '/api/timelog/{action}/[{id}/]', \Gfinance\Api\Controller\TimelogController::class],
    [['GET', 'POST'], '/api/project/{action}/[{id}/]', \Gfinance\Api\Controller\ProjectController::class],
    [['GET', 'POST'], '/api/user/{action}/[{id}/]', \Gfinance\Api\Controller\UserController::class],

];
