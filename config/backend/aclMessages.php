<?php

return [
    \Skeletor\Core\Acl\Acl::MSG_NOT_LOGGED_IN => 'You must be logged in to access private area.',
    \Skeletor\Core\Acl\Acl::MSG_NO_PERMISSIONS => 'You do not have permissions to access %s.',
];