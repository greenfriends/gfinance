<?php

$guest = [
    '/',
    '/login/loginForm/',
    '/login/login/',
    '/login/resetPassword*',
    '/login/forgotPassword*',
    '/login/resetPassword',
    '/ticketing/*/',
    '/jira/*',
    '/user/resetPassword/',
    '/git/bbHook',
    '/api/authenticate/',
    '/api/refresh/',
    '/api/task/*',
    '/api/project/*',
    '/api/user/*',
    '/api/timelog/*'
];

$level2 = [
    '/user/form/*',
    '/user/update/*',
    '/login/logout/',
    '/task/*',
    '/feature/*',
    '/project/view/',
    '/client/view/',
    '/project/view/',
    '/project/form/*',
    '/timelog/*',
    '/watchlist/*/',
    '/api/authenticate/',
    '/api/refresh/',
    '/tag/*',
    '/comment/*',
    '/deploy/*'
];

$level1 = [
    '/user/*',
    '/project/*',
    '/client/*',
    '/invoice/*',
];

//can also see everything level2 can see
$level1 = array_merge($level2, $level1);

return [
    0 => $guest,
    1 => $level1,
    2 => $level2
];