<?php

return array(
    'baseUrl' => 'http://gfinance.local',
    'redirectUri' => '/task/view/',
    'timezone' => 'Europe/Belgrade',
    'appName' => 'GFinance',
    'adminPath' => '',
    'mailer' => [
        'from' => 'milos.jovanov@greenfriends.systems',
        'supportMail' => 'milos.jovanov@greenfriends.systems',
        'fromName' => 'GFinance',
        'recipients' => [
            'errorNotice' => [
                'milos.jovanov@greenfriends.systems',
            ],
            'general' => [
                'milos.jovanov@greenfriends.systems',
            ],
        ],
        'forgotPassword' => [
            'subject' => 'Resetovanje lozinke sa %s'
        ],
        'server' => [],
    ],
);