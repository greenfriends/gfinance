<?php

use Skeletor\User\Repository\UserRepository;
use Laminas\Session\SessionManager;
use Laminas\Session\ManagerInterface;
use Laminas\Session\Config\SessionConfig;
use Monolog\ErrorHandler;
use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Core\Acl\Acl;
use \League\Flysystem\Filesystem;
use League\Plates\{Engine, Template\Theme, Extension\ExtensionInterface};

class GF_Mail_Log_Handler extends \Monolog\Handler\AbstractHandler
{
    /**
     * @var \Skeletor\Mailer\Service\Mailer
     */
    private $mail;

    public function __construct($level = \Monolog\Level::Error, bool $bubble = true)
    {
        parent::__construct($level, $bubble);
    }

    public function setMail(\Skeletor\Mailer\Service\Mailer $mail)
    {
        $this->mail = $mail;
    }

    public function handle(\Monolog\LogRecord $record): bool
    {
        if ($record->level >= \Monolog\Level::Error) {
            $this->mail->handleApplicationError($record);
        }

        return false;
    }
}

$containerBuilder = new \DI\ContainerBuilder;
/* @var \DI\Container $container */
$container = $containerBuilder
//    ->addDefinitions(require_once __DIR__ . '/config_web.php')
    ->build();

$container->set(\Skeletor\User\Repository\UserRepositoryInterface::class, function() use ($container) {
    return $container->get(\Skeletor\User\Repository\UserRepository::class);
});

$container->set(Engine::class, function() use ($container) {
    $defaultTheme = APP_PATH . '/vendor/dj_avolak/skeletor/themes/admin';
    $theme = APP_PATH . '/themes/admin';
    $plates = new \League\Plates\Engine($theme);
    $plates->addFolder('defaultTheme', $defaultTheme, true);
    $plates->addFolder('layout', APP_PATH . '/themes/admin/layout');
    $plates->addFolder('partialsGlobal', APP_PATH . '/themes/admin/partials/global');
    $plates->addFolder('partialsGlobalDefault', $defaultTheme . '/partials/global');
    $plates->registerFunction('printError', function($error, $label) use($plates) {
        return $plates->render('partialsGlobal::error', ['error' => $error, 'label' => $label]);
    });
    $plates->registerFunction('formToken', function () { return \Volnix\CSRF\CSRF::getHiddenInputString(); });
//    $plates->registerFunction('t', function ($string) { return $string; });
    $translator = $container->get(\Skeletor\Translator\Service\Translator::class);
//    $translator(\Skeletor\Translator\Service\Translator::LANGUAGE_SERBIAN);
//    $translator(\Skeletor\Translator\Service\Translator::LANGUAGE_ENGLISH);
    $plates->loadExtension($translator);
    $plates->loadExtension($container->get(\Skeletor\Image\Service\MediaLibraryImageView::class));
    $plates->loadExtension($container->get(\Skeletor\Core\Service\ValuesGeneratorView::class));

    return $plates;
});

$container->set(\FastRoute\Dispatcher::class, function() use ($container) {
    $adminPath = $container->get(Config::class)->adminPath;
    $routeList = require CONFIG_PATH . '/routes.php';

    /** @var \FastRoute\Dispatcher $dispatcher */
    return FastRoute\simpleDispatcher(
        function (\FastRoute\RouteCollector $r) use ($routeList) {
            foreach ($routeList as $routeDef) {
                $r->addRoute($routeDef[0], $routeDef[1], $routeDef[2]);
            }
        }
    );
});

$container->set(Filesystem::class, function() use ($container) {
    $adapter = new League\Flysystem\Local\LocalFilesystemAdapter(APP_PATH);

    return new Filesystem($adapter);
});

$container->set(Acl::class, function() use ($container) {
    return new Acl(
        $container->get(ManagerInterface::class),
        $container->get(Config::class),
        require CONFIG_PATH . '/acl.php',
        require CONFIG_PATH . '/aclMessages.php'
    );
});

$container->set(Config::class, function() use ($container) {
    $params = include(CONFIG_PATH . "/config.php");
    $config = new \Laminas\Config\Config($params);
    $config = $config->merge(new \Laminas\Config\Config(include(CONFIG_PATH . "/config-local.php")));

    return $config;
});

$container->set(Skeletor\Core\Middleware\MiddlewareInterface::class, function() use ($container) {
    return new \Skeletor\Core\Middleware\AuthMiddleware(
        $container->get(ManagerInterface::class),
        $container->get(Config::class),
        $container->get(Flash::class),
        $container->get(Acl::class),
        $container->get(\Skeletor\User\Repository\UserRepositoryInterface::class)
    );
});

$container->set(\Skeletor\User\Model\UserFactoryInterface::class, function() use ($container) {
    return new \Skeletor\User\Model\UserFactory($container->get(\DateTime::class));
});

$container->set(\Skeletor\User\Repository\UserRepositoryInterface::class, function() use ($container) {
    return $container->get(\Skeletor\User\Repository\UserRepository::class);
});

$container->set(ManagerInterface::class, function() use ($container) {
    $sessionConfig = new SessionConfig();
    $redisHost = array_keys($container->get(Config::class)->redis->hosts->toArray())[0];
    $redisPort = array_values($container->get(Config::class)->redis->hosts->toArray())[0];
    $sessionConfig->setOptions([
        'remember_me_seconds' => 2592000, //2592000, // 30 * 24 * 60 * 60 = 30 days
        'use_cookies'         => true,
        'name'                => 'gfinance',
//        'cookie_lifetime'     => 2 * 24 * 60 * 60, // 2 days
        'phpSaveHandler'      => 'redis',
        'savePath'            => sprintf('tcp://%s:%s?weight=1&timeout=1', $redisHost, $redisPort),
    ]);
    $session = new SessionManager($sessionConfig);
    $session->start();

    return $session;
});

$container->set(\Skeletor\Core\Action\Web\NotFoundInterface::class, function() use ($container) {
    return $container->get(\Skeletor\Core\Action\Web\NotFound::class);
});

$container->set(Logger::class, function() use ($container) {
    $logger = new \Monolog\Logger('gfinance');
    $date = $container->get(\DateTime::class);
    $logDir = APP_PATH . '/data/logs/';
    $logSubDir = $logDir . $date->format('Y') . '-' . $date->format('m');
    $logFile = $logSubDir . '/' . gethostname() . '-backend-' . $date->format('d') . '.log';
    $debugLog = $logDir . gethostname() . '-backend-debug.log';
    // create dir or file if needed
    if (!is_dir($logDir)) {
        mkdir($logDir);
    }
    if (!is_dir($logSubDir)) {
        mkdir($logSubDir);
    }
    if (!is_file($logFile)) {
        touch($logFile);
    }
    $logger->pushHandler(
        new StreamHandler($debugLog,\Monolog\Level::Info)
    );
    $logger->pushHandler(
        new StreamHandler($logFile, \Monolog\Level::Error, false)
    );
    $env = strtolower(getenv('APPLICATION_ENV'));
    if ($env && strtolower($env) === 'production') {
        $mailHandler = new GF_Mail_Log_Handler(\Monolog\Level::Error, true);
        $mailHandler->setMail($container->get(\Skeletor\Core\Mailer\Service\Mailer::class));
        $logger->pushHandler($mailHandler);
    }
    if ($env !== 'production') {
        if (php_sapi_name() == "cli") {
            $logger->pushHandler(new \Monolog\Handler\ErrorLogHandler());
        } else {
            $logger->pushHandler(new BrowserConsoleHandler());
        }
    }
    ErrorHandler::register($logger);

    return $logger;
});

$container->set(\DateTime::class, function() use ($container) {
    $dt = new \DateTime('now', new \DateTimeZone($container->get(Config::class)->offsetGet('timezone')));
    return $dt;
});

$container->set(Flash::class, function () use ($container) {
    //session needs to be started for flash
    $container->get(ManagerInterface::class);

    return new Flash();
});

$container->set(\Laminas\Mail\Transport\TransportInterface::class, function() use ($container) {
    $transport = new \Laminas\Mail\Transport\Smtp();
    $options = new \Laminas\Mail\Transport\SmtpOptions($container->get(Config::class)->mailer->server->toArray());
    $transport->setOptions($options);

    return $transport;
});

$container->set(\GuzzleHttp\Client::class, function() {
    return new GuzzleHttp\Client(['verify' => false ]);
});

$container->set(\Skeletor\Login\Provider\ProviderInterface::class, function() use ($container) {
    return new \Skeletor\Login\Provider\DbProvider(
        $container->get(\Skeletor\User\Repository\UserRepository::class)
    );
});

$container->set(\Skeletor\Tag\Repository\TagRepoInterface::class, function() use ($container) {
    return $container->get(\Gfinance\Tag\Repository\Tag::class);
});

$container->set(\Skeletor\Login\Validator\ResetPasswordInterface::class, function() use ($container) {
    return $container->get(\Skeletor\Login\Validator\ResetPasswordLoose::class);
});

$container->set(\Redis::class, function() use ($container) {
    $config = $container->get(Config::class);
    $redis = new \Redis();
    foreach ($config->redis->hosts as $host => $port) {
        $redis->connect($host, $port);
    }
    return $redis;
});

$container->set(\Doctrine\ORM\EntityManagerInterface::class, function() use ($container) {
    $config = \Doctrine\ORM\ORMSetup::createAttributeMetadataConfiguration(
        paths: [
            APP_PATH . "/packages/Client/src/Entity",
            APP_PATH . "/packages/Project/src/Entity",
            APP_PATH . "/packages/Task/src/Entity",
            APP_PATH . "/packages/Tag/src/Entity",
            APP_PATH . "/vendor/dj_avolak/skeletor/src/User",
            APP_PATH . "/vendor/dj_avolak/skeletor/src/Login",
            APP_PATH . "/vendor/dj_avolak/skeletor/src/Translator",
        ],
        isDevMode: true,
    );
    $config->setAutoGenerateProxyClasses(true);
    $resultCache = new Symfony\Component\Cache\Adapter\RedisTagAwareAdapter($container->get(\Redis::class));
//    $config->setResultCache($resultCache);
//    $config->setMetadataCache($resultCache);
//    $config->setHydrationCache($resultCache);
    $dbConfig = $container->get(Config::class);
    $connection = \Doctrine\DBAL\DriverManager::getConnection([
        'dbname' => $dbConfig->db->write->name,
        'user' => $dbConfig->db->write->user,
        'password' => $dbConfig->db->write->pass,
        'host' => $dbConfig->db->write->host,
        'driver' => 'pdo_mysql',
    ], $config);
    $eventManager = new \Doctrine\Common\EventManager();
    \Doctrine\DBAL\Types\Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');

    $em = new \Doctrine\ORM\EntityManager($connection, $config, $eventManager);
    MacFJA\Tracy\DoctrineSql::init($em);

    return $em;
});


return $container;