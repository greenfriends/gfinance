<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']):?>
            <h1 class="page-header"><?=$this->t('Edit tag:')?> <?=$data['model']->getTitle()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create tag:')?></h1>
        <?php endif; ?>
    </div>
</div>
<form action="/product/<?= ($data['model']) ? 'update/' . $data['model']?->getId() : 'create' ?>/"
      class="inlineForm" method="POST" id="userForm" autocomplete="off">
    <?php echo $this->formToken(); ?>
    <div class="form-group">
        <div class="inputContainer">
            <label for="title"><?=$this->t('Title')?></label>
            <input name="title" id="title" class="formInput form-control" value="<?=$data['model']?->getTitle() ?? ''?>"/>
        </div>
    </div>

    <div class="form-group">
        <input type="hidden" name="stickerOption" value="0" />
        <input type="hidden" name="stickerLabel" value="" />
        <input type="hidden" name="priceLabel" value="" />
        <input type="hidden" name="stickerImagePosition" value="<?=\Skeletor\Tag\Model\Tag::POSITION_LEFT?>" />
    </div>

    <div class="form-group">
        <input type="hidden" name="tagId" value="<?= $data['model']?->getId() ?>">
        <input data-action="/tag/<?= ($data['model']) ? 'update/' . $data['model']?->getId() : 'create' ?>/" id="submitButton" type="submit" value="<?= $this->t('Save') ?>"
               class="btn btn-success submitButtonSize"/>
    </div>
</form>