<?php $this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">View comments for task <?=$data['task']->getCode()?></h1>
    </div>
</div>
<?= $this->fetch('task/tabs', ['task' => $data['task'], 'page' => 'comment']) ?>

<?php if ($this->engine->exists('partialsGlobal::dataTable')) {
    $tpl = $this->fetch('partialsGlobal::dataTable', ['data' => $data]);
} else {
    $tpl = $this->fetch('partialsGlobalDefault::dataTable', ['data' => $data]);
} ?>
<?=$this->section('dataTable', $tpl)?>