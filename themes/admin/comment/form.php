<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']?->getId()):?>
            <h1 class="page-header"><?=$this->t('Edit comment')?> <?=$data['model']?->getId()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create comment')?></h1>
        <?php endif; ?>
    </div>
</div>
<?php if ($data['model']) {
    $userId = $data['model']->getUser()->getId();
} else {
    $userId = $loggedIn;
}?>
<form action="<?=$data['path']?>" class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group divUser">
                        <label class="userLabel">Comment</label>
                        <textarea id="body" class="form-control inputDesign" rows="7"
                          name="body"><?=$data['model']?->getBody()?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?=$data['model']?->getid()?>"/>
                        <input id="taskId" type="hidden" name="taskId" value="<?=$data['model']?->getTaskId()?>"/>
                        <input type="hidden" name="userId" value="<?=$userId?>"/>
                        <input type="submit" value="Save" class="btn btn-success submitButtonSize"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>