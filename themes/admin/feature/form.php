<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']?->getId()):?>
            <h1 class="page-header"><?=$this->t('Edit feature')?> <?=$data['model']->getName()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create feature')?></h1>
        <?php endif; ?>
    </div>
</div>
<form action="<?=$data['path']?>" class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group divUser">
                <label class="userLabel">Name</label>
                <input name="name" value="<?=$data['model']?->getName()?>" class="form-control inputDesign" />
            </div>
        </div>
        <div class="col-lg-4">
            <label class="userLabel"><?=$this->t('Project')?></label>
            <select class="form-control selectDesign" name="projectId">
                <?php foreach ($data['projects'] as $project): ?>
                <option value="<?=$project->getId()?>" <?php if ($data['model']?->getProject()->getId() === $project->getid()) {
                    echo 'selected'; }?>><?=$project->getName()?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="id" value="<?=$data['model']?->getId()?>"/>
                <input data-action="<?=$data['path']?>" type="submit" value="<?=$this->t('Submit')?>"
                       class="btn btn-success submitButtonSize" id="submitButton" />
            </div>
        </div>
    </div>
</form>