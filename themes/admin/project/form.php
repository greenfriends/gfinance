<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']?->getId()):?>
            <h1 class="page-header"><?=$this->t('Edit project')?> <?=$data['model']->getName()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create project')?></h1>
        <?php endif; ?>
    </div>
</div>
<form action="<?=$data['path']?>" class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group divUser">
                <label class="userLabel">Name</label>
                <input name="name" value="<?=$data['model']?->getName()?>" class="form-control inputDesign" />
            </div>
            <div class="form-group divUser">
                <label class="userLabel">Code</label>
                <input name="code" value="<?=$data['model']?->getCode()?>" class="form-control inputDesign" />
            </div>
            <div class="form-group divUser">
                <label class="userLabel">Type</label>
                <select name="type">
                    <option value="I" <?php if ($data['model']?->getType() == "I"): ?>selected<?php endif ?>>In</option>
                    <option value="O" <?php if ($data['model']?->getType() == "O"): ?>selected<?php endif ?>>Out</option>
                </select>
            </div>
            <div class="form-group divUser">
                <label class="userLabel">Client</label>
                <select name="clientId" id="clientId" required>
                <?php foreach ($data['clients'] as $client): ?>
                    <option value="<?=$client->getId()?>" <?php if ($data['model']?->getClient()->getId() == $client->getId()): ?>selected<?php endif ?>><?=$client->getName()?></option>
                <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-lg-2">
        </div>
        <div class="col-lg-4">
            <div class="form-group divUser">
                <label class="userLabel">Assigned users</label>
                <select name="assignedUsers[]" id="assignedUsers" multiple>
                <?php foreach ($data['users'] as $user): ?>
                    <option value="<?=$user->getId()?>" <?php if ($data['model']?->isUserAssigned($user)): ?>selected<?php endif ?>><?=$user->getDisplayName()?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <label class="userLabel"><?=$this->t('Status')?></label>
            <select class="form-control selectDesign" name="isActive">
                <option value="1" <?php if ($data['model']?->getIsActive()) { echo 'selected'; }?>
                ><?=$this->t('Active')?></option>
                <option value="0" <?php if ($data['model']?->getIsActive() === 0) { echo 'selected'; }?>
                ><?=$this->t('Inactive')?></option>
            </select>
        </div>
    </div>
    <div class="row">
        <?php
        $production = false;
        $staging = false;
        if ($data['model'] && count($data['model']->getDeploy())) {
            if ($data['model']->getDeploy()['staging']) {
                $staging = $data['model']->getDeploy()['staging'];
            }
            if ($data['model']->getDeploy()['production']) {
                $production = $data['model']->getDeploy()['production'];
            }
        }
        ?>
        <div class="col-lg-6">
            <h3>Deploy info - Production</h3>
            <div class="form-group divUser row">
                <div class="col-lg-6">
                    <label class="userLabel">Deploy project code</label>
                    <input name="deploy[production][projectCode]" value="<?php if ($production) { echo $production->getProjectCode(); } ?>" class="form-control inputDesign" />
                    <?php if ($production && strlen($production->getProjectCode())): ?>
                    <input type="button" value="Deploy to PRODUCTION" id="runDeployProduction" class="btn btn-success"
                           data-url="<?=$data['triggerUrl']['production']?>" />
                <?php endif; ?>
                </div>
                <div class="col-lg-6">
                <label class="userLabel">Deploy branch</label>
                <input name="deploy[production][branch]" value="<?php if ($production) { echo $production->getBranch(); } ?>" class="form-control inputDesign" />
                </div>
            </div>
            <div>
                <?php if (isset($data['deployLog']['production'])) { echo $data['deployLog']['production']; } ?>
            </div>
        </div>
        <div class="col-lg-6">
            <h3>Deploy info - Staging</h3>
            <div class="form-group divUser row">
                <div class="col-lg-6">
                    <label class="userLabel">Deploy project code</label>
                    <input name="deploy[staging][projectCode]" value="<?php if ($staging) { echo $staging->getProjectCode(); } ?>" class="form-control inputDesign" />
                <?php if ($staging && strlen($staging->getProjectCode())): ?>
                    <input type="button" value="Deploy to STAGING" id="runDeployStaging" class="btn btn-success"
                           data-url="<?=$data['triggerUrl']['staging']?>" />
                <?php endif; ?>
                </div>
                <div class="col-lg-6">
                    <label class="userLabel">Deploy branch</label>
                    <input name="deploy[staging][branch]" value="<?php if ($staging) { echo $staging->getBranch(); } ?>" class="form-control inputDesign" />
                </div>
            </div>
            <div>
                <?php if (isset($data['deployLog']['staging'])) { echo $data['deployLog']['staging']; } ?>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <div class="form-group">
                    <input type="hidden" name="id" value="<?=$data['model']?->getId()?>" />
                    <input data-action="<?=$data['path']?>" type="submit" value="<?=$this->t('Submit')?>"
                           class="btn btn-success submitButtonSize" id="submitButton" />
                </div>
            </div>
        </div>
    </div>
</form>