<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']?->getId()):?>
            <h1 class="page-header"><?=$this->t('Edit client')?> <?=$data['model']->getName()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create client')?></h1>
        <?php endif; ?>
    </div>
</div>
<form action="<?=$data['path']?>" class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group divUser">
                <label class="userLabel">Name</label>
                <input name="name" value="<?=$data['model']?->getName()?>" class="form-control inputDesign" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group divUser">
                <label class="userLabel">Code</label>
                <input name="code" value="<?=$data['model']?->getCode()?>" class="form-control inputDesign" />
            </div>
        </div>
        <div class="col-lg-4">
            <label class="userLabel"><?=$this->t('Status')?></label>
            <select class="form-control selectDesign" name="isActive">
                <option value="1" <?php if ($data['model']?->getIsActive()) { echo 'selected'; }?>
                ><?=$this->t('Active')?></option>
                <option value="0" <?php if ($data['model']?->getIsActive() === 0) { echo 'selected'; }?>
                ><?=$this->t('Inactive')?></option>
            </select>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="id" value="<?=$data['model']?->getId()?>"/>
                <input data-action="<?=$data['path']?>" type="submit" value="<?=$this->t('Submit')?>"
                       class="btn btn-success submitButtonSize" id="submitButton" />
            </div>
        </div>
    </div>
</form>