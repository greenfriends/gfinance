<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']?->getId()):?>
            <h1 class="page-header"><?=$this->t('Edit task')?> <?=$data['model']?->getName()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create task')?></h1>
        <?php endif; ?>
    </div>
</div>
<?php
$taskId = $data['model']?->getId();
if (isset($data['formData']['name'])) {
    $name = $data['formData']['name'];
} else {
    $name = $data['model']?->getName();
}
if (isset($data['formData']['name'])) {
    $description = $data['formData']['description'];
} else {
    $description = $data['model']?->getDescription();
}
if (isset($data['formData']['projectId'])) {
    $projectId = (int) $data['formData']['projectId'];
} else {
    $projectId = $data['model']?->getProject()->getId();
}
if (isset($data['formData']['priority'])) {
    $priority = (int) $data['formData']['priority'];
} else {
    $priority = $data['model']?->getPriority();
}
if (isset($data['formData']['status'])) {
    $status = (int) $data['formData']['status'];
} else {
    $status = $data['model']?->getStatus();
}
if (isset($data['formData']['createdBy'])) {
    $createdBy = (int) $data['formData']['createdBy'];
} elseif ($data['model']) {
    $createdBy = $data['model']->getCreatedby()->getId();
} else {
    $createdBy = $loggedIn;
}
if (isset($data['formData']['assignedUsers'])) {
    $assignedUsers = explode(',', $data['formData']['assignedUsers']);
} elseif ($data['model']) {
    $assignedUsers = [];
    foreach ($data['model']->getAssignedUsers() as $user) {
        $assignedUsers[] = $user->getId();
    }
} else {
    $assignedUsers = [];
}
//if (isset($data['formData']['attachments'])) {
//    $attachments = $data['formData']['attachments'];
//} elseif ($data['model']) {
//    $attachments = $data['model']->getAttachments();
//} else {
    $attachments = [];
//}
?>
<?= $this->fetch('task/tabs', ['task' => $data['model'], 'page' => 'task']) ?>
<form action="<?=$data['path']?>" class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-5">
            <div class="row">
                <div class="col-lg-3">
                    <label class="userLabel">Project</label>
                    <select name="projectId" id="projectId" required class="form-control selectDesign">
                        <option disabled selected value>--- Choose project ---</option>
                        <?php foreach ($data['projects'] as $key => $value): ?>
                            <option value="<?=$key?>" <?php if ($key === $projectId) { echo 'selected'; }?>><?=$value?></option>
                        <?php endforeach; ?>
                    </select>
                    <label class="userLabel">Priority</label>
                    <select name="priority" id="priority" <?php if ($status > 6) { echo 'readonly'; } ?> class="form-control selectDesign">
                        <?php foreach ($data['priorityList'] as $key => $value): ?>
                            <option value="<?=$key?>" <?php if ($key === $priority) { echo 'selected'; }?>><?=$value?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="userLabel">Status</label>
                    <select name="status" id="status" <?php if ($taskId) { echo 'selected'; } ?> class="form-control selectDesign">
                        <?php foreach ($data['statusList'] as $key => $value): ?>
                            <option value="<?=$key?>" <?php if ($status == $key) { echo 'selected'; } ?>><?=$value?></option>
                        <?php endforeach; ?>
                    </select>
                    <label class="userLabel">Feature</label>
                    <select name="feature" id="featureId" <?php if ($taskId) { echo 'selected'; } ?> class="form-control selectDesign">
                        <option>Select Feature</option>
                        <?php foreach ($data['features'] as $key => $value): ?>
                            <option value="<?=$key?>" <?php if ($data['model']?->getFeature()?->getId() == $key) { echo 'selected'; } ?>><?=$value?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="userLabel">Reported By</label>
                    <select name="createdBy" id="createdBy" required class="form-control selectDesignr">
                        <?php foreach ($data['users'] as $key => $value): ?>
                            <option value="<?=$key?>" <?php if ($createdBy === $key) { echo 'selected'; }?>><?=$value?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-lg-3">
                    <label class="userLabel">Assigned users</label>
                    <select multiple name="assignedUsers[]" id="assignedUsers" required class="form-control selectDesign">
                        <?php foreach ($data['users'] as $key => $value): ?>
                            <option value="<?=$key?>"
                                <?php foreach ($assignedUsers as $userId): ?>
                                    <?php if ($userId == $key) { echo 'selected'; } ?>
                                <?php endforeach; ?>
                            ><?=$value?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <label class="userLabel">Title</label>
                    <input name="name" value="<?=$name?>" class="form-control inputDesign" />
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label class="userLabel">Description</label>
                    <div id="contentContainer" class="postEditor"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-7 row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="userLabel">Timelog</label>
                        <input name="timelog[timeLogged][]" value="" class="form-control inputDesign" placeholder="Time to log in hours" />
                        <input type="hidden" name="timelog[id][]" value=""/>
                        <input type="hidden" name="timelog[userId][]" value="" />
                    </div>
                    <div class="col-lg-6">
                        <label for="tagSelect"><?=$this->t('Select tag')?></label>
                        <select class="form-control" id="tagSelect">
                            <option style="background:lightgray; color:white;" selected disabled value="-1"><?=$this->t('Choose a tag')?></option>
                            <?php foreach($data['tags'] as $tag): ?>
                                <option value="<?=$tag->getId()?>"><?=$tag->getTitle()?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div id="tagsContainer">
                    </div>
                </div>
                <div class="form-group divUser">
                    <label class="userLabel">Timelog comment</label>
                    <textarea id="" class="form-control inputDesign" rows="2"
                              name="timelog[comment][]"></textarea>
                </div>

                <?php /* @var \Gfinance\Task\Model\TimeLog $log */
                if ($data['model']) {
                foreach ($data['model']->getTimelog() as $log): ?>
                    <p><?=$log->getUpdatedAt()->format('d/m/Y H:i')?>, user <?=$log->getUser()->getDisplayName()?> logged <?=$log->getTimeLogged('h')?> hours.</p>
                    <p><?=$log->getComment()?></p>
                    <div id="existingTagsContainer">
                    <?php foreach($log->getTags() as $tag): ?>
                        <div class="tagContainer" data-tag-id="<?=$tag->getId()?>">
                            <input name="timelog[tags][]" type="hidden" value="<?=$tag->getId()?>">
                            <span><?=$tag->getTitle()?></span>
                            <div class="deleteTag">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                </svg>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <input type="hidden" name="timelog[comment][]" value="<?=$log->getComment()?>" />
                    <input type="hidden" name="timelog[id][]" value="<?=$log->getId()?>" />
                    <input type="hidden" name="timelog[timeLogged][]" value="<?=$log->getTimeLogged('h')?>" />
                    <input type="hidden" name="timelog[userId][]" value="<?=$log->getUser()->getId()?>" />
                <?php endforeach; } ?>
            </div>
            <div class="col-lg-6">
                <div class="form-group divUser">
                    <label class="userLabel">Comments</label>
                    <textarea id="newComment" class="form-control inputDesign comment" rows="2" placeholder="Add new comment"
                          name="comment[body][]"></textarea>
                    <input type="hidden" name="comment[id][]" value=""/>
                    <?php if ($data['model']) {
                    foreach ($data['model']->getComments() as $comment): ?>
                    <textarea class="form-control inputDesign comment" rows="2" name="comment[body][]"
                          ><?=$comment->getBody()?></textarea>
                    <input type="hidden" name="comment[id][]" value="<?=$comment->getid()?>" />
                    <input type="hidden" name="comment[userId][]" value="<?=$comment->getUser()->getid()?>" />
                    <?php endforeach; } ?>
                </div>
<!--                <label class="userLabel">Attachments</label><br />
                <ul>
                    <?php if ($data['model']): ?>
                        <?php foreach ($attachments as $attachment): ?>
                            <li><a target="_blank" href="<?=\Gfinance\Task\Model\Attachment::STORAGE_PATH .
                                $taskId .'/'. $attachment->getFilename()?>"><?=$attachment->getTitle()?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>-->
                <input type="file" name="attachments[]" />
            </div>

        </div>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="id" value="<?=$taskId?>" />
                <input data-action="<?=$data['path']?>" type="submit" value="<?=$this->t('Submit')?>"
                       class="btn btn-success submitButtonSize" id="submitButton" />
            </div>
        </div>
    </div>
    <?php if($data['model']):?>
        <div class="blockData" data-blocks="<?=htmlentities(json_encode($data['model']?->getBlocks() ?? ''))?>"></div>
    <?php endif;?>
</form>
<script>
document.addEventListener('DOMContentLoaded', () => {
    window.startTinyStandard('#description')
});
</script>