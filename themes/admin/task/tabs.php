<?php $taskId = 0;
$commentCount = 0;
$timelogCount = 0;
if ($task) {
    $taskId = $task->getId();
//    $commentCount = count($task->getComments());
    $commentCount = 0;
//    $timelogCount = count($task->getTimelog());
    $timelogCount = 0;
} ?>
<div id="tabbedContentContainer">
    <div id="tabs">
        <a class="tab <?php if ($page === 'task') { echo 'active';} ?>" href="/task/form/<?=$taskId?>/"><?=$this->t('Task info')?></a>
<?php if ($task) { ?>
        <a class="tab <?php if ($page === 'comment') { echo 'active';} ?>" href="/comment/viewForTask/<?=$taskId?>/?taskId=<?=$taskId?>"><?=$this->t('Comments')?> (<?=$commentCount?>)</a>
        <a class="tab <?php if ($page === 'timelog') { echo 'active';} ?>" href="/timelog/viewForTask/<?=$taskId?>/?taskId=<?=$taskId?>"><?=$this->t('Timelogs')?> (<?=$timelogCount?>)</a>
<?php } ?>
    </div>
</div>