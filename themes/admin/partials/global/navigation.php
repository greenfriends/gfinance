<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-text mx-3">GFinance</div>
    </a>

<?php if($loggedIn): ?>
    <!-- Heading -->
    <div class="sidebar-heading"><?=$this->t('Dashboard')?></div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/task/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Tasks')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/project/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Projects')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/feature/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Features')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/deploy/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Deploy')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/client/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Clients')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/watchlist/index/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Watchlist')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/tag/view/">
            <i class="fas fa-users"></i>
            <span><?=$this->t('Tag')?></span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <?php if($loggedInRole === \Skeletor\User\Model\User::ROLE_ADMIN): ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="/user/view/">
                <i class="fas fa-users"></i>
                <span><?=$this->t('Users')?></span>
            </a>
        </li>
<?php /*    <li class="nav-item">
        <a class="nav-link collapsed" href="/activity/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Activities')?></span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/translator/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Translations')?></span>
        </a>
    </li>
 */ ?>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <?php endif; ?>

<?php endif; ?>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>