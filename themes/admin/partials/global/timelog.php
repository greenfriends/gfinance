<div class="row">
    <div class="col-lg-12">
    <?php if ($model->getStatus() < 7): ?>
        <a href="/timelog/form/<?=$model->getId()?>/<?=$timelog->getId()?>/" title="Edit time log" class="startModal">Edit</a> |
    <?php endif; ?>
        <span><?=$timelog->getTimeLogged('h')?> hours</span> |
        <span>by</span>
        <span><?=$timelog->getUser()->getDisplayName(); ?></span> |
        <span style="font-size: 10px">@</span>
        <span style="font-size: 12px"><?=$timelog->getCreatedAt()->format('d/m/Y H:i') ?></span>
        <span style=""><?=$timelog->getComment()?></span>
    </div>
</div>