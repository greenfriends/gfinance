<div class="row">
    <div class="col-lg-12">
        <span>{{ comment.body }}</span>
        <span>by</span>
        <span>{{ comment.user.name }}</span>
        <span>@</span>
        <span>{{ comment.createdAt.format('d/m/Y H:i') }}</span>
    </div>
</div>
