<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=$this->t('View deploy')?> <?=$data['model']?->getId()?></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group divUser">
                            <label class="userLabel">Project</label>
                            <b><?=$data['project']->getName()?></b>
                        </div>
                        <div class="form-group divUser">
                            <label class="userLabel">Trigger url</label>
                            <b class="triggerUrl"><?=$data['triggerUrl']?></b>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div id="triggerUrl" class="row">
                            <input type="button" value="Run Deploy" class="btn btn-success runDeploy" />
                        </div>
                        <div class="row">
                            <h3>Deploy log</h3>
                            <div>
                                <p><?=$data['deployLog']?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
