<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']?->getId()):?>
            <h1 class="page-header"><?=$this->t('Edit timelog')?> <?=$data['model']?->getId()?></h1>
        <?php else: ?>
            <h1 class="page-header"><?=$this->t('Create timelog')?></h1>
        <?php endif; ?>
    </div>
</div>
<?php if ($data['model']) {
    $userId = $data['model']->getUser()->getId();
} else {
    $userId = $loggedIn;
}?>
<form action="<?=$data['path']?>" class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <label class="userLabel">Time to log</label>
                            <input name="timeLogged" value="<?=$data['model']?->getTimeLogged('h')?>h" class="form-control inputDesign"  />
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group divUser">
                                <label class="userLabel">Comment</label>
                                <textarea id="timelogComment" class="form-control inputDesign" rows="7"
                                          name="comment"><?=$data['model']?->getComment()?></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?=$data['model']?->getId()?>"/>
                                <input id="taskId" type="hidden" name="taskId" value="<?=$data['model']?->getTaskId()?>"/>
                                <input type="hidden" name="userId" value="<?=$userId?>"/>
                                <input type="submit" value="Save" class="btn btn-success submitButtonSize"/>
                            </div>
                        </div>
                    </div>

                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.row -->
    </div>
</form>
<script>
    jQuery(document).ready(function() {
        startTinyBare('#timelogComment');
    });
</script>