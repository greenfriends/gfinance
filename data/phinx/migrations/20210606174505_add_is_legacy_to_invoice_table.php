<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddIsLegacyToInvoiceTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `invoice` 
        ADD COLUMN `isLegacy` INT(1) UNSIGNED NULL DEFAULT 0;";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("ALTER TABLE `invoice`
        DROP COLUMN isLegacy;");
    }
}
