<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddPriorityToTaskTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `task` 
        ADD COLUMN `priority` INT(3) UNSIGNED NULL DEFAULT 2 AFTER `updatedAt`;";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("ALTER TABLE `task`
        DROP COLUMN priority;");
    }
}
