<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddInvoiceInfoToProject extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `project` 
        ADD COLUMN invoiceRate VARCHAR (256) DEFAULT NULL,
        ADD COLUMN invoiceDescription VARCHAR (256) DEFAULT NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("ALTER TABLE `project`
        DROP COLUMN invoiceRate,
        DROP COLUMN invoiceDescription;");
    }
}
