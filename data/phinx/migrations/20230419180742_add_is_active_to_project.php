<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddIsActiveToProject extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `project` 
        ADD COLUMN `isActive` INT(1) NULL DEFAULT 0;";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("ALTER TABLE `project`
        DROP COLUMN isActive;");
    }
}
