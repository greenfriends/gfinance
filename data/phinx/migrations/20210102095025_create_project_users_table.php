<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateProjectUsersTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `project_user` (
  `projectUserId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `projectId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`projectUserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `project_user`");
    }
}
