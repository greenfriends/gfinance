<?php

use Phinx\Migration\AbstractMigration;

final class CreateLanguageTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `language` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(8) NOT NULL,
  `code` varchar(8) NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        // insert default languages
        $this->query("INSERT INTO `language` (`name`, `code`) VALUES ('english', 'en-en');");
        $this->query("INSERT INTO `language` (`name`, `code`) VALUES ('serbian', 'sr-sr');");
    }

    public function down()
    {
        $this->query("DROP TABLE `language`");
    }
}
