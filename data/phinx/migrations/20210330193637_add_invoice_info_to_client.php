<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddInvoiceInfoToClient extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `client` 
        ADD COLUMN invoiceName VARCHAR (256) DEFAULT NULL,
        ADD COLUMN invoiceAddress VARCHAR (256) DEFAULT NULL,
        ADD COLUMN invoiceCity VARCHAR (256) DEFAULT NULL,
        ADD COLUMN invoiceCountry VARCHAR (256) DEFAULT NULL,
        ADD COLUMN pib VARCHAR (32) DEFAULT NULL,
        ADD COLUMN mb VARCHAR (32) DEFAULT NULL;";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("ALTER TABLE `client`
        DROP COLUMN invoiceName,
        DROP COLUMN invoiceAddress,
        DROP COLUMN invoiceCity,
        DROP COLUMN invoiceCountry,
        DROP COLUMN pib,
        DROP COLUMN mb;");
    }
}
