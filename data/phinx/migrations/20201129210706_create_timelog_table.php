<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTimelogTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `timeLog` (
  `timeLogId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `taskId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `timeLogged` int(10) NOT NULL,
  `comment` TEXT NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`timeLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `timeLog`");
    }
}
