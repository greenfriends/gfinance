<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddInvoiceIdToTaskTable extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE `task` 
        ADD COLUMN `invoiceId` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `updatedAt`;";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("ALTER TABLE `task`
        DROP COLUMN invoiceId;");
    }
}
