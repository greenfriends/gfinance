<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateInvoiceTables extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `invoice` (
  `invoiceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceNumber` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL,
  `clientId` int(10) unsigned NOT NULL,
  `projectId` int(10) unsigned NOT NULL,
  `createdBy` int(10) unsigned NOT NULL,
  `status` int(4) NOT NULL,
  `type` int(4) NOT NULL,
  `issueDate` datetime NOT NULL,
  `turnoverDate` datetime NOT NULL,
  `deadlineDate` datetime NOT NULL,
  `exchangeRate` double(10,4) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`invoiceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `invoice`");
    }
}
