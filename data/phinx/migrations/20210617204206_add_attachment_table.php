<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddAttachmentTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `attachment` (
  `attachmentId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` VARCHAR(128) NOT NULL,
  `title` VARCHAR(256) NOT NULL,
  `description` TEXT NULL,
  `type` int(1) NOT NULL,
  `taskId` int(10) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`attachmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `attachment`");
    }
}
