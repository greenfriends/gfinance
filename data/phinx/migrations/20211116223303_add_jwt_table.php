<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddJwtTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `jwt` (
  `jwtId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `expiresAt` datetime,
  `source` varchar(256) NULL,
  `jwt` varchar(512) NOT NULL,
  `userId` int(10) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`jwtId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `jwt`");
    }
}
