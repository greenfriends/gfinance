<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class MigrateNamingToIdOnly extends AbstractMigration
{
    public function up()
    {
        $this->query("ALTER TABLE `client` CHANGE COLUMN `clientId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `comment` CHANGE COLUMN `commentId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `deploy` CHANGE COLUMN `deployId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `project` CHANGE COLUMN `projectId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `project_user` CHANGE COLUMN `projectUserId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `task` CHANGE COLUMN `taskId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `timeLog` CHANGE COLUMN `timeLogId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `user` CHANGE COLUMN `userId` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `user` 
    ADD COLUMN `ipv4` INT UNSIGNED,
    ADD COLUMN `lastLogin` datetime,
    ADD COLUMN `firstName` varchar(128) NULL,
    ADD COLUMN `lastName` varchar(128) NULL");
    }

    public function down()
    {
        $this->query("ALTER TABLE `client` CHANGE COLUMN `id` `clientId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `comment` CHANGE COLUMN `id` `commentId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `deploy` CHANGE COLUMN `id` `deployId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `project` CHANGE COLUMN `id` `projectId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `project_user` CHANGE COLUMN `id` `projectUserId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `task` CHANGE COLUMN `id` `taskId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `timeLog` CHANGE COLUMN `id` `timeLogId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `user` CHANGE COLUMN `id` `userId` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
        $this->query("ALTER TABLE `user` 
DROP COLUMN ipv4,
DROP COLUMN lastLogin,
DROP COLUMN firstName,
DROP COLUMN lastName");
    }
}
