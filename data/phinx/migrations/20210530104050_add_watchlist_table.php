<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class AddWatchlistTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `watchlist` (
  `watchlistId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `projectId` int(10) NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`watchlistId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `watchlist`");
    }
}
