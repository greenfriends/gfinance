<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateDeployTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `deploy` (
  `deployId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectId` int(10) unsigned NOT NULL,
  `projectCode` varchar(128) NOT NULL,
  `branch` varchar(64) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`deployId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `deploy`");
    }
}
