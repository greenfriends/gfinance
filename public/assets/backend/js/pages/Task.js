import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/pageEvents.js";
import PostEditor from "../postEditor/PostEditor.js";
import ValuesGenerator from "../valuesGenerator/ValuesGenerator.js";

export default class Task extends CrudPage {
    constructor() {
        super();
        this.dataTableOptions = {
            enableCheckboxes: true,
            shiftCheckboxModifier: true
        };
        this.modalOptions = {
            createModalWidth: '100%',
            createModalHeight: '100%',
            editModalWidth: '100%',
            editModalHeight: '100%'
        }
        this.defaultSort = [
            {order: 'DESC', orderBy: 'createdAt'},
            {order: 'DESC', orderBy: 'updatedAt'}
        ];

        this.addFormReadyListeners();
    }

    addFormReadyListeners() {
        this.eventEmitter.on(pageEvents.entityFormReady, (data) => {
            this.tagSelect = document.getElementById('tagSelect');
            this.tagsContainer = document.getElementById('tagsContainer');
            this.addTagHandle();
            this.initPostEditor();

            if(data && data.action && data.action === 'edit') {
                const blockData = this.getBlockData();
                if(blockData) {
                    this.postEditor.preloadBlocks(blockData);
                }
            }
        });
    }

    getBlockData() {
        const blockDataElem = document.querySelector('.blockData');
        if(blockDataElem && blockDataElem.getAttribute('data-blocks')) {
            return JSON.parse(blockDataElem.getAttribute('data-blocks'));
        }
        return null;
    }

    initPostEditor() {
        this.postEditorContainer = document.getElementById('contentContainer');
        if(this.postEditorContainer) {
            this.postEditor = new PostEditor(this.postEditorContainer, [], this.blockConfig);
            this.postEditor.init();
        }
    }

    loadValueGenerators() {
        const form = this.modal.getFormInModal();
        if(form) {
            let valuesGeneratorContainers = ValuesGenerator.getAllContainersForInit(form);
            if(valuesGeneratorContainers) {
                valuesGeneratorContainers.forEach((container) => {
                    const valuesGenerator = new ValuesGenerator(container, form);
                    valuesGenerator.init();
                });
            }
        }
    }

    addTagHandle() {
        this.tagSelect.addEventListener('change', () => {
            let selectedOption = this.disableSelectedTagOption();
            this.tagSelect.value = '-1';
            console.log(this.tagsContainer);
            this.tagsContainer.appendChild(this.generateTagEntity(selectedOption.innerText, selectedOption.value));
        });

        // this.handleExistingTags();
    }

    handleExistingTags() {
        let existingTags = document.querySelectorAll('.tagContainer');
        existingTags.forEach((tag) => {
            let value = tag.getAttribute('data-tag-id');
            this.disableSelectedTagOption(value);
            tag.querySelector('.deleteTag').addEventListener('click', () => {
                this.enableTagOption(value);
                tag.remove();
            });
        });
    }

    disableSelectedTagOption(value = null) {
        let selectedOption;
        if(value) {
            selectedOption = this.tagSelect.querySelector(`option[value="${value}"]`);
        } else {
            selectedOption = this.tagSelect.querySelector(`option[value="${this.tagSelect.value}"]`);
        }
        selectedOption.disabled = true;
        selectedOption.style.background = 'lightgray';
        selectedOption.style.color = 'white';
        return selectedOption;
    }

    enableTagOption(value) {
        let option = this.tagSelect.querySelector(`option[value="${value}"]`);
        option.disabled = false;
        option.style.background = 'white';
        option.style.color = '#6e707e';
    }

    generateTagEntity(name, value) {
        let tagContainer = document.createElement('div');
        tagContainer.classList.add('tagContainer');
        let input = document.createElement('input');
        //check if we deleted an existing tag previously
        let deletedTagInput = document.querySelector(`.deletedTag[value="${value}"]`);
        input.name = `timelog[tags][]`;
        input.value = value;
        input.type = 'hidden';
        tagContainer.appendChild(input);
        let text = document.createElement('span');
        text.innerText = name;
        tagContainer.appendChild(text);

        let deleteElement = document.createElement('div');
        deleteElement.classList.add('deleteTag');
        deleteElement.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                      </svg>`;
        deleteElement.addEventListener('click', () => {
            tagContainer.remove();
            this.enableTagOption(value);
        });
        if(deletedTagInput) {
            deletedTagInput.remove();
        }
        tagContainer.appendChild(deleteElement);
        return tagContainer;
    }

    generateTagToBeAdded(value, name) {
        let templateContent = document.getElementById('tagToBeAddedTemplate').content.cloneNode(true);
        let tagName = templateContent.querySelector('span');
        tagName.innerText = name;
        templateContent.querySelector('.tagToBeAddedEntity').setAttribute('data-id', value);

        let input = templateContent.querySelector('input');
        input.value = name;
        input.name = `tagsToBeAdded[${value}]`;
        let deleteButton = templateContent.querySelector('.deleteTagToBeAdded');
        deleteButton.addEventListener('click', () => {
            deleteButton.parentElement.remove();
        });
        return templateContent;
    }

    getTagDataFromSelect() {
        this.tagsSelectBulk = document.getElementById('tagSelectBulk');
        this.tags = [];
        Array.from(this.tagsSelectBulk.options).forEach((option) => {
            if(option.value === '-1') {
                return;
            }
            this.tags.push({id: option.value, value: option.innerText});
        });
    }

}