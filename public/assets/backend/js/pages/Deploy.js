import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/pageEvents.js";
export default class Deploy extends CrudPage {
    constructor() {
        super();
        this.modalOptions = {
            closeOnEscape: true,
            closeOnClickOutsideOfModal: true,
            warnUserIfFormEditedOnModalClose: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'createdAt'},
            {order: 'DESC', orderBy: 'updatedAt'}
        ];

        this.eventEmitter.on(pageEvents.entityFormReady, (data) => {
            $('#triggerUrl').on('click', '.runDeploy', function(event) {
                $.getScript($('.triggerUrl').text() + "&callback=parseDeployTriggerResponse");
            });
        });
    }

}