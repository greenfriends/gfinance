import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/pageEvents.js";
export default class ProjectPage extends CrudPage {
    constructor() {
        super();
        this.modalOptions = {
            closeOnEscape: true,
            closeOnClickOutsideOfModal: true,
            warnUserIfFormEditedOnModalClose: true,
            createModalWidth: '100%',
            createModalHeight: '100%',
            editModalWidth: '100%',
            editModalHeight: '100%'
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'createdAt'},
            {order: 'DESC', orderBy: 'updatedAt'}
        ];
        this.eventEmitter.on(pageEvents.entityFormReady, (data) => {
            let triggerProd = document.getElementById('runDeployProduction');
            let triggerStage = document.getElementById('runDeployStaging');
            if (triggerProd) {
                triggerProd.addEventListener('click', () => {
                    this.fetchData(triggerProd.dataset.url, 'GET').then((response) => {
                        if (response.status) {
                            alert('Deploy to PRODUCTION triggered successfully.');
                        }
                    }).then(() => { console.log('done'); });
                });
            }
            if (triggerStage) {
                triggerStage.addEventListener('click', () => {
                    this.fetchData(triggerStage.dataset.url, 'GET').then((response) => {
                        if (response.status) {
                            alert('Deploy to STAGING triggered successfully.');
                        }
                    }).then(() => { console.log('done'); });
                });
            }
        });
    }

    async fetchData(url, method, params = null) {
        const req = await fetch(url, {
            method: method,
            body: params
        });
        return JSON.parse(await req.text());
    }
}