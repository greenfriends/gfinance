import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
export default class Tag extends CrudPage {
    constructor() {
        super();
        this.modalOptions = {
            closeOnEscape: true,
            closeOnClickOutsideOfModal: true,
            warnUserIfFormEditedOnModalClose: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'createdAt'},
            {order: 'DESC', orderBy: 'updatedAt'}
        ];
    }
}