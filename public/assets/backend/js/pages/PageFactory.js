import User from "./User.js";
import ClientPage from "./Client.js";
import ProjectPage from "./Project.js";
import Deploy from "./Deploy.js";
import Task from "./Task.js";
import Comment from "./Comment.js";
import Timelog from "./Timelog.js";
import Tag from "./Tag.js";
import Feature from "./Feature.js";

import ActivitiesCrudPage from "./Activities.js";

export default class PageFactory {
    static make(page) {
        switch(page) {
            case 'User':
                return new User();
            case 'Client':
                return new ClientPage();
            case 'Project':
                return new ProjectPage();
            case 'Deploy':
                return new Deploy();
            case 'Task':
                return new Task();
            case 'Comment':
                return new Comment();
            case 'Timelog':
                return new Timelog();
            case 'Tag':
                return new Tag();
            case 'Feature':
                return new Feature();

            case 'Activity':
                return new ActivitiesCrudPage();
        }
    }
}