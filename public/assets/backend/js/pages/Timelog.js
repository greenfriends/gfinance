import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/pageEvents.js";

export default class Timelog extends CrudPage {
    constructor() {
        super();
        this.modalOptions = {
            closeOnEscape: true,
            closeOnClickOutsideOfModal: true
        };
        this.dataTableOptions = {
            enableCheckboxes: true,
            shiftCheckboxModifier: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'createdAt'},
            {order: 'DESC', orderBy: 'updatedAt'}
        ];
        this.addAdditionalListeners();
    }

    addAdditionalListeners() {
        this.eventEmitter.on(pageEvents.entityFormReady, (data) => {
            this.setTaskIdFromUrl();
            // window.startTinyStandard('#body');
        });
    }

    // fetch taskId from url
    setTaskIdFromUrl() {
        const params = new Proxy(new URLSearchParams(window.location.search), {
            get: (searchParams, prop) => searchParams.get(prop),
        });
        let taskId = params.taskId;
        if (document.getElementById('taskId')) {
            document.getElementById('taskId').value = taskId;
        }
    }
}