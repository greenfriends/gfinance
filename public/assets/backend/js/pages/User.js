import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
import {dataTablesAssets} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/DataTable/dataTablesAssets.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/pageEvents.js";
import {dataTableEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/DataTable/dataTableEvents.js";

export default class User extends CrudPage {
    constructor() {
        super();
        this.dataTableOptions = {
            enableCheckboxes: true,
            shiftCheckboxModifier: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'createdAt'},
            {order: 'DESC', orderBy: 'updatedAt'}
        ];
    }
}