import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";
import {pageEvents} from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/pageEvents.js";
export default class PosCrudPage extends CrudPage {
    getHPPTemplateEndpoint = '/sp-office/pos/getHPPTemplate/';
    preview = null;
    constructor() {
        super();
        this.modalOptions = {
            closeOnEscape: true,
            closeOnClickOutsideOfModal: true,
            warnUserIfFormEditedOnModalClose: true
        };
        this.defaultSort = [
            {order: 'DESC', orderBy: 'created_at'},
            {order: 'DESC', orderBy: 'updated_at'}
        ];
        this.addAdditionalListeners();
        // this.initPreview();
    }

    setBaseAction() {
        const urlParts = window.location.pathname.split('/');
        this.baseAction = urlParts[1] +'/'+ urlParts[2];
    }

    addAdditionalListeners() {
        this.eventEmitter.on(pageEvents.entityFormReady, (data) => {
            this.addPreviewListener();
            this.addImagePreviewDataListener();
        });
    }

    initPreview() {
        this.preview = new Preview({
            previewModal: document.getElementById(posSelectors.ids.previewModal),
            closePreviewModal: document.getElementById(posSelectors.ids.closePreviewModal),
            previewModalContent: document.getElementById(posSelectors.ids.previewContent),
            POSTemplatePreview: document.getElementById(posSelectors.ids.POSTemplatePreview),
            getHPPTemplateEndpoint: this.getHPPTemplateEndpoint
        });
        this.preview.init();
    }

    addPreviewListener() {
        let previewButton = this.modal.getInnerModal().querySelector(`#${posSelectors.ids.previewButton}`);
        if (previewButton) {
            previewButton.addEventListener('click', (e) => {
                e.preventDefault();
                this.preview.open();
                this.preview.populate(new POSFormModel(this.modal.getFormInModal()));
            });
        }
    }

    addImagePreviewDataListener() {
        let imageInput = this.modal.getFormInModal().querySelector(`input[name="${posSelectors.attributes.imageInputName}"]`);
        if(imageInput) {
            imageInput.addEventListener('change', (e) => {
                let file = imageInput.files[0];
                let reader = new FileReader();
                reader.addEventListener('load', () => {
                    imageInput.setAttribute(posSelectors.attributes.dataPreviewImage, reader.result.toString());
                }, false);
                if (file) {
                    reader.readAsDataURL(file);
                }
            });
        }
    }
}