import CrudPage from "https://skeletor.greenfriends.systems/dtables/1.x/0.1/src/Page/CrudPage.js";

export default class ActivitiesCrudPage extends CrudPage {
    constructor() {
        super();
        this.modalOptions = {
            closeOnEscape: true,
            closeOnClickOutsideOfModal: true,
            warnUserIfFormEditedOnModalClose: false
        };
    }

    setBaseAction() {
        const urlParts = window.location.pathname.split('/');
        this.baseAction = urlParts[1] +'/'+ urlParts[2];
    }
}