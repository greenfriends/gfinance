import PageFactory from "./pages/PageFactory.js";
import {dataTableSelectors} from "https://skeletor.greenfriends.systems/dtables/1.x/dev/src/DataTable/dataTableSelectors.js";
$(document).ready(function() {

    window.parseDeployTriggerResponse = function (data) {
        if (data.status) {
            $.notify("Deploy started.", {className:'success', autoHide: true, autoHideDelay: 10000});
        } else {
            $.notify("Another deploy is already scheduled.", {className:'error', autoHide: true, autoHideDelay: 10000});
        }
    }

    function startChart(analyticsData, analyticsLabels) {
        var ctx = document.getElementById('myChart');

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: analyticsLabels,
                datasets: analyticsData
            },
            options: {
                maintainAspectRatio: false
            },
            plugins: {
                legend: {
                    position: 'top'
                },
                title: {
                    display: true,
                    text: 'A Damn Good Chart!'
                }
            }
        });
    }

});

window.startTinyBare = function (selector) {
    var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

    tinymce.init({
        selector: selector,
        toolbar_mode: 'floating',
        plugins: 'paste searchreplace autolink visualblocks link lists ',
        // menubar: 'edit view insert format table',
        toolbar: 'undo redo | bold italic underline | fontselect fontsizeselect | numlist bullist | forecolor backcolor removeformat',
        height: 300,
        skin: useDarkMode ? 'oxide-dark' : 'oxide',
        content_css: useDarkMode ? 'dark' : 'default',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
}

window.startTinyStandard = function (selector) {
    var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
    tinymce.remove();

    tinymce.init({
        selector: selector,
        // plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
        // plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        plugins: 'print paste searchreplace autolink code visualblocks visualchars link codesample table insertdatetime advlist lists wordcount quickbars emoticons',
        // imagetools_cors_hosts: ['picsum.photos'],
        // menubar: 'file edit view insert format tools table help',
        menubar: 'edit view insert format table',
        toolbar: 'undo redo | bold italic underline | fontselect fontsizeselect | numlist bullist | forecolor backcolor removeformat | fullscreen  preview save print | insertfile image media template link anchor codesample',
        toolbar_sticky: true,
        height: 400,
        // image_caption: true,
        // quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        // noneditable_noneditable_class: 'mceNonEditable',
        // toolbar_mode: 'sliding',
        // contextmenu: 'link image imagetools table',
        skin: useDarkMode ? 'oxide-dark' : 'oxide',
        content_css: useDarkMode ? 'dark' : 'default',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
}

if (document.getElementById(dataTableSelectors.ids.table)) {
    document.addEventListener('DOMContentLoaded', () => {
        const pageIdentifier = document.getElementById(dataTableSelectors.ids.table).getAttribute(dataTableSelectors.attributes.page);
        console.log(pageIdentifier);
        const pageEntity = PageFactory.make(pageIdentifier);
        console.log(pageEntity);
        pageEntity.init();
    });
}
