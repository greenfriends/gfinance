export const commandEvents = Object.freeze({
   commandClicked: 'commandClicked',
   updateContent: 'updateContent'
});