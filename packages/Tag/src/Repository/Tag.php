<?php
namespace Gfinance\Tag\Repository;


class Tag extends \Skeletor\Tag\Repository\Tag
{
    const ENTITY = \Gfinance\Tag\Entity\Tag::class;
    const FACTORY = \Gfinance\Tag\Factory\TagFactory::class;

    function getSearchableColumns(): array
    {
        return ['title'];
    }
}