<?php
namespace Gfinance\Tag\Factory;

use Doctrine\ORM\EntityManagerInterface;

class TagFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $tag = $em->getRepository(\Gfinance\Tag\Entity\Tag::class)->find($data['id']);
        $tag->populateFromDto(new \Skeletor\Tag\Model\Tag(...$data));

        return $tag->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $tag = new \Gfinance\Tag\Entity\Tag();
        $data['id'] = \Ramsey\Uuid\Uuid::uuid4();
        $tag->populateFromDto(new \Skeletor\Tag\Model\Tag(...$data));
        $em->persist($tag);

        return $tag->getId();
    }

    public static function make($itemData, EntityManagerInterface $em): \Skeletor\Tag\Model\Tag
    {
        return new \Skeletor\Tag\Model\Tag(...$itemData);
    }
}