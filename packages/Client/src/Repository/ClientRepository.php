<?php
namespace Gfinance\Client\Repository;

use Gfinance\Client\Entity\Client;
use Skeletor\Core\TableView\Repository\TableViewRepository;

class ClientRepository extends TableViewRepository
{
    const ENTITY = \Gfinance\Client\Entity\Client::class;
    const FACTORY = \Gfinance\Client\Factory\ClientFactory::class;

    public function getSearchableColumns(): array
    {
        return ['name'];
    }

    public function codeExists(string $code): bool
    {
        $client = $this->entityManager->getRepository(Client::class)->findOneBy(['code' => $code]);
        if ($client) {
            return true;
        }
        return false;
    }

    public function nameExists(string $name): bool
    {
        $client = $this->entityManager->getRepository(Client::class)->findOneBy(['name' => $name]);
        if ($client) {
            return true;
        }
        return false;
    }
}
