<?php
namespace Gfinance\Client\Validator;

use Laminas\Validator\EmailAddress;
use Gfinance\Client\Repository\ClientRepository;
use Volnix\CSRF\CSRF;

class Client
{
    /**
     * @var ClientRepository
     */
    private $repo;

    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * @param ClientRepository $userRepo
     * @param CSRF $csrf
     */
    public function __construct(ClientRepository $repo, CSRF $csrf)
    {
        $this->repo = $repo;
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;
        if (!$data['id'] && $this->repo->nameExists($data['name'])) {
            $this->messages['name'][] = 'Name already exists in system and must be unique.';
            $valid = false;
        }

        if (strlen($data['name']) < 1) {
            $this->messages['name'][] = 'Name must be entered.';
            $valid = false;
        }

        if (strlen($data['code']) !== 2 && strlen($data['code']) !== 3) {
            $this->messages['code'][] = 'Code must be 2 or 3 characters long.';
            $valid = false;
        }

        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}