<?php
namespace Gfinance\Client\Model;

use Skeletor\Core\Model\Model;

class Client extends Model
{
    public function __construct(
        private string $id, private string $name, private string $code, private int $isActive, $createdAt = null, $updatedAt = null
    ) {
        parent::__construct($createdAt, $updatedAt);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}