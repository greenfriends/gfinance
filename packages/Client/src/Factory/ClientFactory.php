<?php
namespace Gfinance\Client\Factory;

use Gfinance\Client\Entity\Client;

class ClientFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $client = $em->getRepository(Client::class)->find($data['id']);
        $client->populateFromDto(new \Gfinance\Client\Model\Client(...$data));

        return $client->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $client = new Client();
        $data['id'] = \Ramsey\Uuid\Uuid::uuid4();
        $client->populateFromDto(new \Gfinance\Client\Model\Client(...$data));
        $em->persist($client);

        return $client->getId();
    }

    public static function make($itemData, $em)
    {
        return new \Gfinance\Client\Model\Client(...$itemData);
    }
}