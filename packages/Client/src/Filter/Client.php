<?php
namespace Gfinance\Client\Filter;

use Laminas\Filter\ToInt;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Core\Filter\FilterInterface;
use Gfinance\Client\Validator\Client as ClientValidator;
use Skeletor\Core\Validator\ValidatorException;
use Volnix\CSRF\CSRF;

class Client implements FilterInterface
{
    /**
     * @var ClientValidator
     */
    private $validator;

    public function __construct(ClientValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter($postData) : array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $data = [
            'id' => (isset($postData['id'])) ? $postData['id'] : null,
            'name' => $postData['name'],
            'code' => $postData['code'],
            'isActive' => $postData['isActive'],
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            $e = new ValidatorException();
            $e->data = $data;
            throw $e;
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }
}