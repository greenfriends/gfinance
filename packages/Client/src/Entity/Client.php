<?php
namespace Gfinance\Client\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Skeletor\Core\Entity\Timestampable;
use Gfinance\Client\Model\Client as DtoModel;

#[ORM\Entity]
#[ORM\Table(name: 'client')]
class Client
{
    use Timestampable;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $name;

    #[ORM\Column(type: Types::STRING, length: 3)]
    private string $code;

    #[ORM\Column(type: Types::INTEGER, length: 1)]
    private int $isActive;

    public function populateFromDto(DtoModel $dto)
    {
        if ($dto->getId()) {
            $this->id = $dto->getId();
        }
        $this->name = $dto->getName();
        $this->code = $dto->getCode();
        $this->isActive = $dto->getIsActive();

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getId()
    {
        return $this->id;
    }
}