<?php
namespace Gfinance\Client\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Gfinance\Client\Factory\ClientFactory;
use Gfinance\Client\Service\Client;
use Gfinance\Task\Entity\Timelog;
use Gfinance\Task\Factory\TaskFactory;
use Gfinance\Task\Service\Task;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Skeletor\Core\Controller\AjaxCrudController;
use Skeletor\User\Model\UserFactory;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;

class ClientController extends AjaxCrudController
{
    const TITLE_VIEW = "View client";
    const TITLE_CREATE = "Create new client";
    const TITLE_UPDATE = "Edit client: ";
    const TITLE_UPDATE_SUCCESS = "client updated successfully.";
    const TITLE_CREATE_SUCCESS = "client created successfully.";
    const TITLE_DELETE_SUCCESS = "client deleted successfully.";
    const PATH = 'client';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    /**
     * @param Client $client
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     */
    public function __construct(
        Client $client, Session $session, Config $config, Flash $flash, Engine $template
        , private EntityManagerInterface $em, private User $user, private Task $task
    ) {
        parent::__construct($client, $session, $config, $flash, $template);
    }

    public function import()
    {
//        $this->importTasks();
        $this->importTimelog();
    }

    public function importTimelog()
    {
        $dsn = "mysql:host=localhost;dbname=gfinance";
        $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );
        $pdo = new \PDO($dsn, 'root', 'rootpass', $options);
        $users = [
            '3' => 'cd00d656-20b1-4cf3-bfdf-54b09c59e523', // v
            '4' => '1fb53324-c0d6-4f72-ab54-776c656252d6', // p
            '5' => '79f75c4f-b65b-4007-8015-53f05e25f76b', // m
            '11' => '8e159124-6cf1-47ed-84e7-551f8cd705b8', // d
        ];

        foreach ($this->em->getRepository(\Gfinance\Task\Entity\Task::class)->findAll() as $task) {
            $sql = "SELECT * FROM task where id = " . $task->getCode();
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $item = $stmt->fetch(\PDO::FETCH_ASSOC);
            $task->setCreatedAt(new \DateTime($item['createdAt']));
            $task->setUpdatedAt(new \DateTime($item['updatedAt']));
            $description = [[
                'type' => 'textEditor',
                'value' => $item['description'],
            ]];
            $task->setDescription(json_encode($description));

            $sql = "SELECT * FROM timeLog where taskId = " . $task->getCode();
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
//            $logs = new ArrayCollection();
//            foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $logData) {
//                if (!isset($users[$logData['userId']])) {
//                    continue;
//                }
//                $logEntry = new Timelog();
//                $logEntry->setUser($this->em->getRepository(\Skeletor\User\Entity\User::class)->find($users[$logData['userId']]));
//                $logEntry->populateFromDto(new \Gfinance\Task\Model\Timelog(\Ramsey\Uuid\Uuid::uuid4(), $logData['timeLogged'],
//                    $logData['comment'], null, null, new \DateTime($logData['createdAt']), new \DateTime($logData['updatedAt'])));
//                $logs[] = $logEntry;
//                $this->em->persist($logEntry);
//            }
//            if (count($logs)) {
//                $task->setTimelog($logs);
//            }
        }
        $this->em->flush();
    }

    public function importTasks()
    {
        $dsn = "mysql:host=localhost;dbname=gfinance";
        $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );
        $pdo = new \PDO($dsn, 'root', 'rootpass', $options);
        $sql = "SELECT * FROM task where projectId = 37";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();

        $users = [
            '3' => 'cd00d656-20b1-4cf3-bfdf-54b09c59e523', // v
            '4' => '1fb53324-c0d6-4f72-ab54-776c656252d6', // p
            '5' => '79f75c4f-b65b-4007-8015-53f05e25f76b', // m
            '11' => '8e159124-6cf1-47ed-84e7-551f8cd705b8', // d
        ];

//        $internal = '51912615-b48a-427d-8d80-e6f0c21f9633';

        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $assigned = [];
            foreach (explode(',', $row['assignedUsers']) as $userId) {
                if (isset($users[$userId])) {
                    $assigned[] = $users[$userId];
                }
            }

            if (!isset($users[$row['createdBy']])) {
                $user = '8e159124-6cf1-47ed-84e7-551f8cd705b8';
            } else {
                $user = $users[$row['createdBy']];
            }

            $description = sprintf('[{"type":"textEditor","value":"%s"}]', $row['description']);

            TaskFactory::compileEntityForCreate([
                'name' => $row['name'],
                'projectId' => '0789abd4-6d56-482c-b73c-b4d491ac370e',
                'userId' => $user,
                'status' => $row['status'],
                'code' => $row['id'],
                'createdBy' => $user,
                'tags' => new ArrayCollection(),
                'comment' => new ArrayCollection(),
                'timelog' => new ArrayCollection(),
                'priority' => $row['priority'],
                'assignedUsers' => $assigned,
                'description' => $description,
            ], $this->em);
            $this->em->flush();
        }

        echo 'done';
        die();
    }


}