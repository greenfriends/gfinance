<?php
namespace Gfinance\Client\Service;

use Skeletor\Core\TableView\Service\TableView;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\User\Service\Session;
use Gfinance\Client\Repository\ClientRepository as Repository;
use Gfinance\Client\Filter\Client as Filter;

class Client extends TableView
{
    const APPLY_TENANT_FILTER = false;

    /**
     * @param Repository $repo
     * @param Session $userSession
     * @param Logger $logger
     */
    public function __construct(
        Repository $repo, Session $userSession, Logger $logger, Filter $filter
    ) {
        parent::__construct($repo, $userSession, $logger, $filter);
    }

    public function prepareEntities($entities)
    {
        $items = [];
        foreach ($entities as $client) {
            $items[] = [
                'columns' => [
                    'id' => $client->getId(),
                    'name' =>  [
                        'value' => $client->getName(),
                        'editColumn' => true,
                    ],
                    'code' => $client->getCode(),
                    'isActive' => ($client->getIsActive() === 1) ? 'Active':'Inactive',
                    'createdAt' => $client->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $client->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $client->getId(),
            ];
        }
        return $items;
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'code', 'label' => 'Code'],
            ['name' => 'isActive', 'label' => 'Status'],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Created at'],
        ];

        return $columnDefinitions;
    }
}