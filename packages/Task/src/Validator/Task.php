<?php
namespace Gfinance\Task\Validator;

use Volnix\CSRF\CSRF;

class Task
{
    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * @param CSRF $csrf
     */
    public function __construct(CSRF $csrf)
    {
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;
        if (strlen($data['name']) < 3) {
            $this->messages['name'][] = 'Title must be at least 3 characters long';
            $valid = false;
        }
        if (strlen($data['projectId']) === 0) {
            $this->messages['name'][] = 'Project must be selected';
            $valid = false;
        }
        if (count((array) $data['assignedUsers']) === 0) {
            $this->messages['name'][] = 'At least one user must be assigned';
            $valid = false;
        }


        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}