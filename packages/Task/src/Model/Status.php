<?php

namespace Gfinance\Task\Model;

class Status
{
    const STATUS_NEW = 1;
    const STATUS_OPEN = 2;
    const STATUS_IN_PROGRESS = 3;
    const STATUS_HOLD = 4;
    const STATUS_REVIEW = 5;
    const STATUS_WAIT = 6;
    const STATUS_DONE = 7;
    const STATUS_ARCHIVED = 8;

    public static function getHrStatus($id)
    {
        return static::getHrStatuses()[$id];
    }

    public static function getHrStatuses()
    {
        return [
            static::STATUS_NEW => 'New',
            static::STATUS_OPEN => 'Open',
            static::STATUS_IN_PROGRESS =>'In Progress',
            static::STATUS_HOLD => 'On hold',
            static::STATUS_REVIEW => 'Review',
            static::STATUS_WAIT => 'Waiting',
            static::STATUS_DONE => 'Done',
            static::STATUS_ARCHIVED => 'Archived',
        ];
    }
}