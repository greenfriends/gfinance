<?php

namespace Gfinance\Task\Model;

use Skeletor\User\Model\User;
use Skeletor\Core\Model\Model;

class Comment extends Model
{
    private $id;

    private $body;

    private $user;
    private $taskId;

    /**
     * Project constructor.
     * @param $projectId
     * @param $name
     * @param $clientId
     */
    public function __construct(User $user, $id, $body, $taskId = null, $createdAt = null, $updatedAt = null)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->body = $body;
        $this->user = $user;
        $this->taskId = $taskId;
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}