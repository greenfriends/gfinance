<?php

namespace Gfinance\Task\Model;

class Priority
{
    const PRIORITY_LOW = 1;
    const PRIORITY_MEDIUM = 2;
    const PRIORITY_HIGH = 3;

    public static function getHrPriority($id)
    {
        return static::getHrPriorities()[$id];
    }

    public static function getHrPriorities()
    {
        return [
            static::PRIORITY_MEDIUM => 'Standard',
            static::PRIORITY_LOW => 'Low',
            static::PRIORITY_HIGH =>'High',
        ];
    }
}