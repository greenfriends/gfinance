<?php

namespace Gfinance\Task\Model;

use Skeletor\Model\Model;

class Attachment extends Model
{
    const TYPE_MEDIA = 1;
    const TYPE_DOCUMENT = 2;
    const TYPE_UNKNOWN = 3;

    const STORAGE_PATH = '/attachments/';

    private $attachmentId;
    private $filename;
    private $title;
    private $description;
    private $type;
    private $taskId;

    /**
     * Attachment constructor.
     * @param $attachmentId
     * @param $filename
     * @param $title
     * @param $description
     * @param $type
     * @param $taskId
     */
    public function __construct($attachmentId, $filename, $title, $description, $type, $taskId, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->attachmentId = (int) $attachmentId;
        $this->filename = $filename;
        $this->title = $title;
        $this->description = $description;
        $this->type = (int) $type;
        $this->taskId = (int) $taskId;
    }

    public static function getHrTypes()
    {
        return [
            static::TYPE_MEDIA => 'Media',
            static::TYPE_DOCUMENT => 'Document',
            static::TYPE_UNKNOWN =>'Unknown',
        ];
    }

    public function getId()
    {
        return $this->attachmentId;
    }

    /**
     * @return mixed
     */
    public function getAttachmentId()
    {
        return $this->attachmentId;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }
}