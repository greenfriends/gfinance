<?php
namespace Gfinance\Task\Model;

use Skeletor\User\Model\User;
use Skeletor\Core\Model\Model;

class TimeLog extends Model
{
    private $id;

    private $timeLogged;

    private $taskId;

    /**
     * @var User
     */
    private $user;

    private $comment;

    /**
     * Timelog constructor.
     * @param $timeLogId
     * @param $timeLogged
     * @param $task
     * @param $user
     * @param $comment
     */
    public function __construct(
        $id, $timeLogged, $comment, ?User $user = null, $taskId = null, $tags = [], $createdAt = null, $updatedAt = null)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->id = $id;
        $this->timeLogged = $timeLogged;
        $this->taskId = $taskId;
        $this->user = $user;
        $this->tags = $tags;
        $this->comment = $comment;
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTimeLogged($format = 's')
    {
        if ($format !== 's') {
            switch ($format) {
                case 'm':
                return round($this->timeLogged / 60, 4);
                    break;
                case 'h':
                return round($this->timeLogged / (60 * 60), 4);
                    break;
                case 'd':
                return round($this->timeLogged / (60 * 60 * 24), 4);
                    break;
            }
        }
        return $this->timeLogged;
    }

    /**
     * @return mixed
     */
    public function getTaskId()
    {
        return $this->taskId;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }
}