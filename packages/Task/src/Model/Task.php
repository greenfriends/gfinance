<?php
namespace Gfinance\Task\Model;

use Doctrine\Common\Collections\Collection;
use Gfinance\Client\Model\Client;
use Gfinance\Project\Model\Feature;
use Gfinance\Project\Model\Project;
use Gfinance\Task\Model\Priority;
use Skeletor\Core\Model\Model;
use Skeletor\User\Model\User;

class Task extends Model
{
    public function __construct(
        private string $id, private string $name, private string $description, private int $status, private int $priority,
        private ?string $code = null, private ?User $createdBy = null, private ?Collection $assignedUsers = null,
        private ?Collection $comments = null, private ?Collection $timelog = null, private ?Collection $tags = null,
        private array $blocks = [], private ?Feature $feature = null,
//        private ?Collection $attachments = null,
        private ?Client $client = null, private ?Project $project = null, $createdAt = null, $updatedAt = null
    )
    {
        parent::__construct($createdAt, $updatedAt);
    }

    public function getFeature()
    {
        return $this->feature;
    }

    public function getBlocks()
    {
        return $this->blocks;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getAssignedUsers()
    {
        return $this->assignedUsers;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return array
     */
    public function getTimelog()
    {
        return $this->timelog;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

//    public function getCode()
//    {
//        return sprintf('%s-%s-%s', $this->getProject()->getCode(), $this->getProject()->getType(), $this->getId());
//    }

    public function getPriority()
    {
        return $this->priority;
    }
}