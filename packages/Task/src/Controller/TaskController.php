<?php
namespace Gfinance\Task\Controller;

use Gfinance\Client\Service\Client;
use Gfinance\Project\Service\Feature;
use Gfinance\Project\Service\Project;
use Gfinance\Task\Model\Priority;
use Gfinance\Task\Model\Status;
use Gfinance\Task\Service\Task as TaskService;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Skeletor\Core\Controller\AjaxCrudController;
use Skeletor\Tag\Service\Tag;
use Skeletor\User\Model\User as UserModel;
use Skeletor\User\Service\User;
use Skeletor\Core\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;

class TaskController extends AjaxCrudController
{
    const TITLE_VIEW = "View task";
    const TITLE_CREATE = "Create new task";
    const TITLE_UPDATE = "Edit task: ";
    const TITLE_UPDATE_SUCCESS = "task updated successfully.";
    const TITLE_CREATE_SUCCESS = "task created successfully.";
    const TITLE_DELETE_SUCCESS = "task deleted successfully.";
    const PATH = 'task';

    protected $tableViewConfig = ['writePermissions' => true];

    /**
     * @param Client $client
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     */
    public function __construct(
        TaskService $task, Session $session, Config $config, Flash $flash, Engine $template, Project $project,
        User $user, private Client $client, private Tag $tag, private Feature $feature
    ) {
        parent::__construct($task, $session, $config, $flash, $template);
        $this->project = $project;
        $this->user = $user;
    }

    public function form(): Response
    {
        $this->formData['projects'] = $this->project->getFilterData(['isActive' => 1], null, ['name' => 'asc']);
        // @TODO catch change event for project, and select appropriate features
        $this->formData['features'] = $this->feature->getFilterData([], null, ['name' => 'asc']);
        $this->formData['clients'] = $this->client->getFilterData(['isActive' => 1], null, ['name' => 'asc']);
        $this->formData['priorityList'] = Priority::getHrPriorities();
        $this->formData['statusList'] = Status::getHrStatuses();
        $this->formData['users'] = $this->user->getFilterData(['isActive' => 1], null, ['displayName' => 'asc']);
        $this->formData['tags'] = $this->tag->getEntities();

        return parent::form();
    }

    public function analytics(): Response
    {
        $this->setGlobalVariable('pageTitle', 'Task analytics');
        $params = $this->getRequest()->getQueryParams();
        $roleFilter = false;
        $projects = $this->projectRepository->fetchAll();
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== UserModel::ROLE_ADMIN) {
//            $roleFilter = implode(',', $this->projectRepository->getAssignedProjects($this->getLoggedInUserId()));
            $projects = $this->projectRepository->fetchAll(['type' => Project::TYPE_OUT]);
            $ids = [];
            foreach ($projects as $project) {
                $ids[] = $project->getId();
            }
            $roleFilter = implode(',', $ids);
        }
        $data = $this->service->getAnalyticsData($params, $roleFilter);
//        var_dump($data['data']);

        return $this->respond('analytics', [
            'analyticsData' => json_encode($data['data']),
            'analyticsLabels' => json_encode($data['labels']),
            'users' => $this->userService->getRepository()->fetchAll(),
            'projects' => $projects,
            'statusList' => \Gfinance\Timelog\Model\Task\Status::getHrStatus(),
            'priorityList' => \Gfinance\Timelog\Model\Task\Priority::getHrPriorities(),
            'filterUser' => (isset($params['assignedUsers'])) ? $params['assignedUsers'] : 0,
            'filterStatus' => (isset($params['status'])) ? explode(',', $params['status']) : 0,
            'filterProject' => (isset($params['projectId'])) ? $params['projectId'] : 0,
            'filterDateFrom' => (isset($params['dateFrom'])) ? urldecode($params['dateFrom']) : '',
            'filterDateTo' => (isset($params['dateTo'])) ? urldecode($params['dateTo']) : '',
            'type' => (isset($params['typePicker'])) ? $params['typePicker'] : 'monthly',
        ]);
    }

}