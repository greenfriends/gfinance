<?php
namespace Gfinance\Task\Controller;

use Gfinance\Task\Service\Comment;
use Gfinance\Task\Repository\CommentRepository;
use Gfinance\Task\Service\Task;
use Gfinance\Timelog\Controller\Article;
use Gfinance\Timelog\Controller\ClientRepository;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Controller\AjaxCrudController;
use Skeletor\Controller\Controller;
use Skeletor\TableView\Service\TableDecorator;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;

//use SNF\Article\Model\DataTablesRequest;

class CommentController extends AjaxCrudController
{
    const TITLE_VIEW = "View comment";
    const TITLE_CREATE = "Create new comment";
    const TITLE_UPDATE = "Edit comment: ";
    const TITLE_UPDATE_SUCCESS = "comment updated successfully.";
    const TITLE_CREATE_SUCCESS = "comment created successfully.";
    const TITLE_DELETE_SUCCESS = "comment deleted successfully.";
    const PATH = 'comment';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    public function __construct(
        Comment $comment, Session $session, Config $config, Flash $flash, Engine $template, Task $task
    ) {
        parent::__construct($comment, $session, $config, $flash, $template);
        $this->task = $task;
    }

    public function viewForTask(): Response
    {
        $this->setGlobalVariable('pageTitle', static::TITLE_VIEW);
        $tableDecorator = new TableDecorator($this->template, $this->service->compileTableColumns());
        $task = $this->task->getById((int) $this->getRequest()->getAttribute('id'));

        return $this->respond('viewPartial', [
            'tableFilters' => $tableDecorator->generateFiltersHtml(),
            'columnHeaders' => $tableDecorator->generateColumnHeadersForView(),
            'writePermissions' => $this->tableViewConfig['writePermissions'],
            'createAction' => $this->tableViewConfig['createAction'],
            'taskId' => $task->getId(),
            'task' => $task,
            'entityPath' => $this->tableViewConfig['entityPath']
        ]);
    }
}