<?php
namespace Gfinance\Task\Controller;

use Gfinance\Client\Service\Client;
use Gfinance\Task\Repository\TimelogRepository;
use Gfinance\Task\Service\Task;
use Gfinance\Task\Service\Timelog;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Controller\AjaxCrudController;
use Skeletor\TableView\Service\TableDecorator;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;


class TimelogController extends AjaxCrudController
{

    const TITLE_VIEW = "View timelog";
    const TITLE_CREATE = "Create new timelog";
    const TITLE_UPDATE = "Edit timelog: ";
    const TITLE_UPDATE_SUCCESS = "timelog updated successfully.";
    const TITLE_CREATE_SUCCESS = "timelog created successfully.";
    const TITLE_DELETE_SUCCESS = "timelog deleted successfully.";
    const PATH = 'timelog';

    protected $tableViewConfig = ['writePermissions' => true];

    public function __construct(
        Timelog $timelog, Session $session, Config $config, Flash $flash, Engine $template, Task $task
    ) {
        parent::__construct($timelog, $session, $config, $flash, $template);
        $this->task = $task;
    }

    public function viewForTask(): Response
    {
        $this->setGlobalVariable('pageTitle', static::TITLE_VIEW);
        $tableDecorator = new TableDecorator($this->template, $this->service->compileTableColumns());
        $task = $this->task->getById((int) $this->getRequest()->getAttribute('id'));

        return $this->respond('viewPartial', [
            'tableFilters' => $tableDecorator->generateFiltersHtml(),
            'columnHeaders' => $tableDecorator->generateColumnHeadersForView(),
            'writePermissions' => $this->tableViewConfig['writePermissions'],
            'createAction' => $this->tableViewConfig['createAction'],
            'taskId' => $task->getId(),
            'task' => $task,
            'entityPath' => $this->tableViewConfig['entityPath']
        ]);
    }
}