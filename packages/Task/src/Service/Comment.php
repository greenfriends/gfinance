<?php
namespace Gfinance\Task\Service;

use Gfinance\Task\Repository\CommentRepository as Repository;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\TableView\Service\Table as TableView;
use Skeletor\User\Service\Session;

class Comment extends TableView
{
    const APPLY_TENANT_FILTER = false;

    /**
     * @param Repository $repo
     * @param Session $userSession
     * @param Logger $logger
     */
    public function __construct(
        Repository $repo, Session $userSession, Logger $logger
    ) {
        parent::__construct($repo, $userSession, $logger);
    }

    public function update(array $data)
    {
        if ($this->filter) {
            $data = $this->filter->filter($data);
        }
        $oldModel = $this->repo->getById($data['id']);
        $model = $this->repo->update($data);

        return $model;
    }

    public function getEntityData(int $id)
    {
        $entity = $this->repo->getById($id);

        return [
            'id' => $entity->getId(),
        ];
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        $items = [];
        foreach ($data['entities'] as $comment) {
            $items[] = [
                'columns' => [
                    'body' =>  [
                        'value' => $comment->getBody(),
                        'editColumn' => true,
                    ],
                    'userId' => $comment->getUser()->getDisplayName(),
                    'createdAt' => $comment->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $comment->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $comment->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'body', 'label' => 'Text'],
            ['name' => 'userId', 'label' => 'User'],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Created at'],
        ];

        return $columnDefinitions;
    }
}