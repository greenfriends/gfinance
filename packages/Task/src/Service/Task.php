<?php
namespace Gfinance\Task\Service;

use Gfinance\Project\Service\Project;
use Gfinance\Task\Model\Priority;
use Gfinance\Task\Model\Status;
use Gfinance\Task\Repository\TaskRepository;
use Gfinance\Timelog\Service\ifttTrigger;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\User\Service\Session;
use Skeletor\User\Service\User;
use Skeletor\Core\TableView\Service\TableView;

class Task extends TableView
{
    const APPLY_TENANT_FILTER = false;

    public function __construct(
        TaskRepository $repo, Session $session, Client $client, ifttTrigger $ifttTrigger,
        Logger $logger, \Gfinance\Task\Filter\Task $filter, private User $userService, private Project $project
    ) {
        parent::__construct($repo, $session, $logger, $filter);
    }

    public function saveAttachmentFromTicket($fileData, $fileName, $taskId)
    {
        $targetPath = APP_PATH . '/public' . \Gfinance\Task\Model\Attachment::STORAGE_PATH . $taskId;
        if (!is_dir($targetPath)) {
            mkdir($targetPath);
        }
        try {
            file_put_contents($targetPath .'/'. $fileName, $fileData);
            $this->taskRepo->attachmentMapper->insert([
                'filename' => $fileName,
                'title' => $fileName,
                'description' => '',
                'type' => 1,
                'taskId' => $taskId,
            ]);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
    }

    public function saveAttachments($files, $taskId)
    {
        /* @var \GuzzleHttp\Psr7\UploadedFile $file */
        foreach ($files as $file) {
            if ($file->getError() === UPLOAD_ERR_NO_FILE) {
                continue;
            }
            $targetPath = APP_PATH . '/public' . \Gfinance\Task\Model\Attachment::STORAGE_PATH . $taskId;
            if (!is_dir($targetPath)) {
                mkdir($targetPath);
            }
            try {
                $file->moveTo($targetPath .'/'. $file->getClientFilename());
                $this->taskRepo->attachmentMapper->insert([
                    'filename' => $file->getClientFilename(),
                    'title' => $file->getClientFilename(),
                    'description' => '',
                    'type' => 1,
                    'taskId' => $taskId,
                ]);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                var_dump($file->getError());
                die();
            }
        }
    }

    public function getAnalyticsData($params, $roleFilter)
    {
        if (isset($params['dateFrom'])) {
            $params['dateFrom'] = \DateTime::createFromFormat('d/m/Y', $params['dateFrom'])->format('Y-m-d');
        }
        if (isset($params['dateTo'])) {
            $params['dateTo'] = \DateTime::createFromFormat('d/m/Y', $params['dateTo'])->format('Y-m-d');
        }
        $type = \Gfinance\Task\Mapper\Task::ANALYTICS_TYPE_MONTHLY;
        if (isset($params['typePicker']) && $params['typePicker'] !== 'monthly') {
            $type = \Gfinance\Task\Mapper\Task::ANALYTICS_TYPE_DAILY;
        }
        unset($params['typePicker']);
        return $this->repo->fetchAnalyticsData($params, $roleFilter, $type);
    }

    public function prepareEntities($entities)
    {
        $items = [];
        /* @var \Gfinance\Task\Model\Task $task */
        foreach ($entities as $task) {
            $timeLogged = 0;
            foreach ($task->getTimeLog() as $timeLog) {
                $timeLogged += $timeLog->getTimeLogged();
            }
            $assigned = [];
            foreach ($task->getAssignedUsers() as $user) {
                $assigned[] = $user->getDisplayName();
            }
//            var_dump($task->getFeature());
//            die();
            $items[] = [
                'columns' => [
                    'id' => $task->getId(),
                    'code' =>  [
                        'value' => $task->getCode(),
                        'editColumn' => true,
                    ],
                    'name' =>  [
                        'value' => $task->getName(),
                        'editColumn' => true,
                    ],

                    'status' => Status::getHrStatus($task->getStatus()),
                    'priority' => Priority::getHrPriority($task->getPriority()),
                    'project' => $task->getProject()->getName(),
                    'feature' => $task->getFeature()?->getName() ?? '',
                    'timeLogged' => round($timeLogged / 3600, 2) . 'h',
                    'a.assignedUsers' => implode(',', $assigned),
                    'createdAt' => $task->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $task->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $task->getId(),
            ];
        }
        return $items;
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'code', 'label' => 'Code'],
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'project', 'label' => 'Project', 'filterData' => $this->project->getFilterData(['isActive' => 1], null, ['name' => 'asc'])],
            ['name' => 'feature', 'label' => 'Feature'],
            ['name' => 'status', 'label' => 'Status', 'filterData' => Status::getHrStatuses()],
            ['name' => 'timeLogged', 'label' => 'Timer', 'rangeFilter' => ['type' => 'number']],
            ['name' => 'a.assignedUsers', 'label' => 'Assigned', 'filterData' => $this->userService->getFilterData(['isActive' => 1], null, ['displayName' => 'asc'])],
            ['name' => 'updatedAt', 'label' => 'Updated at', 'rangeFilter' => ['type' => 'date']],
//            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'priority', 'label' => 'Priority', 'filterData' => Priority::getHrPriorities()],
        ];

        return $columnDefinitions;
    }
}