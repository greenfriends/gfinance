<?php
namespace Gfinance\Task\Service;

use Gfinance\Task\Repository\TimelogRepository as Repository;
use Gfinance\Timelog\Service\ifttTrigger;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Core\TableView\Service\TableView;
use Skeletor\User\Service\Session;

class Timelog  extends TableView
{
    const APPLY_TENANT_FILTER = false;

    /**
     * @param Repository $repo
     * @param Session $userSession
     * @param Logger $logger
     */
    public function __construct(
        Repository $repo, Session $session, Logger $logger
    ) {
        parent::__construct($repo, $session, $logger);
    }

    public function getEntityData($id)
    {
        $entity = $this->repo->getById($id);

        return [
            'id' => $entity->getId(),
        ];
    }

    public function prepareEntities($entities)
    {
        $items = [];
        /* @var \Gfinance\Task\Model\TimeLog $timelog */
        foreach ($entities as $timelog) {
            $items[] = [
                'columns' => [
                    'comment' =>  [
                        'value' => $timelog->getComment(),
                        'editColumn' => true,
                    ],
                    'timeLogged' => $timelog->getTimeLogged('h') . ' h',
                    'user' => $timelog->getUser()->getDisplayName(),
                    'createdAt' => $timelog->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $timelog->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $timelog->getId(),
            ];
        }
        return $items;
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'comment', 'label' => 'Comment'],
            ['name' => 'timeLogged', 'label' => 'Logged'],
            ['name' => 'user', 'label' => 'User'],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Created at'],
        ];

        return $columnDefinitions;
    }
}