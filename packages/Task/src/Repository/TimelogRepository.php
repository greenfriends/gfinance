<?php
namespace Gfinance\Task\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Gfinance\Timelog\Service\ifttTrigger;
use Skeletor\Core\TableView\Repository\TableViewRepository;

class TimelogRepository extends TableViewRepository
{
    const DAY = 60*60*8;
    const HOUR = 60*60;
    const MINUTE = 60;

    private $ifttTrigger;

    public function __construct(\DateTime $dt, ifttTrigger $ifttTrigger, EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->ifttTrigger = $ifttTrigger;
    }

    private function parseTime($time)
    {
        $time = strtolower(str_replace(' ', '', $time));
        $days = explode('d', $time);
        $loggedTime = 0;
        if (isset($days[1])) {
            $time = $days[1];
            $loggedTime += $days[0] * self::DAY;
        }
        $hours = explode('h', $time);
        if (isset($hours[1])) {
            $time = $hours[1];
            if ($hours[0] > 60) {
                throw new \Exception('Invalid hour format');
            }
            $loggedTime += $hours[0] * self::HOUR;
        }
        $minutes = explode('m', $time);
        if (isset($minutes[1])) {
            $time = $minutes[1];
            if ($minutes[0] > 60) {
                throw new \Exception('Invalid minute format');
            }
            $loggedTime += $minutes[0] * self::MINUTE;
        }
        $seconds = explode('s', $time);
        if (isset($seconds[1])) {
            if ($seconds[0] > 60) {
                throw new \Exception('Invalid seconds format');
            }
            $loggedTime += $seconds[0];
        }
        return $loggedTime;
    }

    public function getSearchableColumns(): array
    {
        return [];
    }
}