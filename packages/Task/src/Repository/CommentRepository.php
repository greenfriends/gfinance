<?php
declare(strict_types=1);

namespace Gfinance\Task\Repository;

use Gfinance\Task\Mapper\Comment as Mapper;
use Gfinance\Task\Model\Comment as Model;
use Laminas\Config\Config;
use Skeletor\Mapper\NotFoundException;
use Skeletor\TableView\Repository\TableViewRepository;
use Skeletor\User\Service\User;


class CommentRepository extends TableViewRepository
{
    /**
     * @var User
     */
    private $userService;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $mapper, User $userService, \DateTime $dt)
    {
        parent::__construct($mapper, $dt);
        $this->userService = $userService;
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }
        $data['user'] = $this->userService->getById($data['userId']);
        unset($data['userId']);

        return new Model(...$data);
    }

    public function getSearchableColumns(): array
    {
        return [];
    }
}
