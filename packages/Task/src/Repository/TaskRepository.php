<?php
namespace Gfinance\Task\Repository;

use Gfinance\Task\Model\Task as Model;
use Skeletor\Core\TableView\Repository\TableViewRepository;
use Skeletor\User\Service\User;

class TaskRepository extends TableViewRepository
{
    const ENTITY = \Gfinance\Task\Entity\Task::class;
    const FACTORY = \Gfinance\Task\Factory\TaskFactory::class;

    public function fetchWithTimeLog($params = array(), int $limit = null, $order = null, $roleFilter = false)
    {
        $items = [];
        if (isset($params['dateFrom'])) {
            $params['dateFrom'] = \DateTime::createFromFormat('d/m/Y', $params['dateFrom'])->format('Y-m-d');
        }
        if (isset($params['dateTo'])) {
            $params['dateTo'] = \DateTime::createFromFormat('d/m/Y', $params['dateTo'])->format('Y-m-d');
        }
        foreach ($this->mapper->fetchWithTimeLog($params, $limit, $order, $roleFilter) as $data) {
            $item = $this->make($data);
            $item->totalTimeLogged = round((int) $data['totalLogged'], 2);
            $items[] = $item;
        }

        return $items;
    }

    public function getByIdWithTimeLog($id): Model
    {
        return $this->make($this->mapper->getByIdWithTimeLog((int) $id));
    }

    public function fetchAnalyticsData($params, $roleFilter, $type)
    {
        $data = $users = $labels = [];
        $colors = [
            3 => '#56007F', // blue
            4 => 'red',
            5 => 'green',
            6 => 'yellow',
            9 => 'brown',
        ];

        foreach ($this->mapper->fetchAnalytics($params, $roleFilter, $type) as $datum) {
            if ((int) $datum['userId'] === 0) {
                continue;
            }
            // cache users for convenience
            if (!isset($users[$datum['userId']])) {
                $users[$datum['userId']] = $this->userService->getById($datum['userId']);
            }
            // get labels
            if (!in_array($datum['period'], $labels)) {
                $labels[] = $datum['period'];
            }
            $data[$datum['period']][$datum['userId']] = $datum['totalLogged'];
        }
//        var_dump($data);
//        die();
        $d = [];
        $userData = [];
        // fill array with 0 where appropriate
        foreach ($data as $period => $datum) {
            $foundUsers = [];
            foreach ($datum as $userId => $value) {
                $foundUsers[] = $userId;
                $userData[$userId][] = $value;
            }
            if (count(array_diff(array_keys($users), $foundUsers)) > 0) {
                foreach (array_diff(array_keys($users), $foundUsers) as $userId) {
                    $userData[$userId][] = 0;
                }
            }
        }
        // finally sort periodic data by user
        foreach ($userData as $userId => $timeLogged) {
            $d[] = [
                'label' => $users[$userId]->getDisplayName(),
                'data' => $timeLogged,
                'backgroundColor' => $colors[$userId],
                'borderColor' => $colors[$userId]
            ];
        }

//        var_dump($d);
//        die();

        return ['data' => $d, 'labels' => $labels];
    }

    public function getSearchableColumns(): array
    {
        return ['name', 'description', 'code'];
    }
}
