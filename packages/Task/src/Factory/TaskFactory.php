<?php
namespace Gfinance\Task\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use Gfinance\Client\Entity\Client;
use Gfinance\Client\Factory\ClientFactory;
use Gfinance\Project\Entity\Feature;
use Gfinance\Project\Entity\Project;
use Gfinance\Project\Factory\FeatureFactory;
use Gfinance\Project\Factory\ProjectFactory;
use Gfinance\Tag\Entity\Tag;
use Gfinance\Task\Entity\Comment;
use Gfinance\Task\Entity\Task;
use Gfinance\Task\Entity\Timelog;
use Skeletor\User\Entity\User;
use Skeletor\User\Model\UserFactory;

class TaskFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $task = $em->getRepository(Task::class)->find($data['id']);
        $project = $em->getRepository(Project::class)->find($data['projectId']);
        $feature = null;
        if ($data['feature']) {
            $feature = $em->getRepository(Feature::class)->find($data['feature']);
        }
        $client = $em->getRepository(Client::class)->find($project->getClient()->getId());
        $userEntity = $em->getRepository(User::class)->find($data['userId']);
        $user = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($userEntity), $em);
        $createdByEntity = $em->getRepository(User::class)->find($data['createdBy']);
        $data['createdBy'] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($createdByEntity), $em);
        $data['code'] = $task->getCode();
        $users = new ArrayCollection();
        foreach ($data['assignedUsers'] as $userId) {
            $users[] = $em->getRepository(User::class)->find($userId);
        }
        $timelog = static::compileTimelog($data['timelog'], $userEntity, $task, $em);
        $comments = static::compileComments($data['comment'], $userEntity, $task, $em, $user);
        unset($data['timelog']);
        unset($data['comment']);
        unset($data['projectId']);
        unset($data['feature']);
        unset($data['clientId']);
        unset($data['userId']);
        unset($data['assignedUsers']);
        $task->populateFromDto(new \Gfinance\Task\Model\Task(...$data));
        $task->setClient($client);
        $task->setProject($project);
        $task->setAssignedUsers($users);
        $task->setComments($comments);
        $task->setTimelog($timelog);
        $task->setCreatedBy($createdByEntity);
        $task->setFeature($feature);

        return $task->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $task = new Task();
        $data['id'] = \Ramsey\Uuid\Uuid::uuid4();
        $project = $em->getRepository(Project::class)->find($data['projectId']);
        $client = $em->getRepository(Client::class)->find($project->getClient()->getId());
        $userEntity = $em->getRepository(User::class)->find($data['userId']);
        $user = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($userEntity), $em);
        $createdByEntity = $em->getRepository(User::class)->find($data['createdBy']);
        $data['createdBy'] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($createdByEntity), $em);
        $feature = null;
        if ($data['feature']) {
            $feature = $em->getRepository(Feature::class)->find($data['feature']);
        }
        $users = new ArrayCollection();
        foreach ($data['assignedUsers'] as $userId) {
            $users[] = $em->getRepository(User::class)->find($userId);
        }
        $timelogData = $data['timelog'];
        $commentsData = $data['comment'];
        unset($data['timelog']);
        unset($data['comment']);
        unset($data['projectId']);
        unset($data['clientId']);
        unset($data['userId']);
        unset($data['feature']);
        unset($data['assignedUsers']);
        $data['code'] = sprintf('%s-%s-%s', $project->getClient()->getCode(), $project->getCode(), $em->getRepository(Task::class)->count([]) + 1);
        $i = 2;
        while ($em->getRepository(Task::class)->count(['code' => $data['code']]) > 0) {
            $data['code'] = sprintf('%s-%s-%s', $project->getClient()->getCode(), $project->getCode(), $em->getRepository(Task::class)->count([]) + $i);
            $i++;
            if ($i > 1000) { // could do worse :D
                die('could not generate id');
            }
        }
        $task->populateFromDto(new \Gfinance\Task\Model\Task(...$data));
        $timelog = static::compileTimelog($timelogData, $userEntity, $task, $em);
        $comments = static::compileComments($commentsData, $userEntity, $task, $em, $user);
        $task->setClient($client);
        $task->setProject($project);
        $task->setAssignedUsers($users);
        $task->setComments($comments);
        $task->setTimelog($timelog);
        $task->setCreatedBy($createdByEntity);
        $task->setFeature($feature);
        $em->persist($task);

        return $task->getId();
    }

    public static function make($data, $em)
    {
        $data['client'] = ClientFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['client']), $em);
        $data['project'] = ProjectFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['project']), $em);
        if ($data['feature']) {
            $data['feature'] = FeatureFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['feature']), $em);
        }
        $data['createdBy'] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['createdBy']), $em);
        $users = new ArrayCollection();
        foreach ($data['assignedUsers'] as $userEntity) {
            $users[] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($userEntity), $em);
        }
        $comments = new ArrayCollection();
        foreach ($data['comments'] as $commentEntity) {
            $comments[] = CommentFactory::make($em->getUnitOfWork()->getOriginalEntityData($commentEntity), $em);
        }
        $timelog = new ArrayCollection();
        foreach ($data['timelog'] as $timelogEntity) {
            $timelog[] = TimelogFactory::make($em->getUnitOfWork()->getOriginalEntityData($timelogEntity), $em);
        }
        $data['blocks'] = [];
        if ($data['description'] === '') {
            $data['description'] = '[]';
        }
        foreach (json_decode($data['description']) as $block) {
            $data['blocks'][] = (array) $block;
        }

        $data['comments'] = $comments;
        $data['timelog'] = $timelog;
        $data['assignedUsers'] = $users;
        unset($data['projectId']);
        unset($data['featureId']);
        unset($data['clientId']);
        unset($data['userId']);
        return new \Gfinance\Task\Model\Task(...$data);
    }


    public static function compileComments($items, $userEntity, $task, $em, $user)
    {
        $comments = new ArrayCollection();
        foreach ($items as $commentData) {
            if ($commentData['id'] !== '') {
                $comment = $em->getRepository(Comment::class)->find($commentData['id']);
            } else {
                $comment = new Comment();
                $commentData['id'] = \Ramsey\Uuid\Uuid::uuid4();
            }
            $comment->setUser($userEntity);
            $comment->populateFromDto(new \Gfinance\Task\Model\Comment($user, $commentData['id'], $commentData['body'], $task->getId()));
            $em->persist($comment);
            $comments[] = $comment;
        }
        return $comments;
    }

    public static function compileTimelog($items, $userEntity, $task, $em)
    {
        $timelog = new ArrayCollection();
        foreach ($items as $timelogData) {
            $tags = new ArrayCollection();
            foreach ($timelogData['tags'] as $tagId) {
                $tags[] = $em->getRepository(Tag::class)->find($tagId);
            }
            if ($timelogData['id'] !== '') {
                $logEntry = $em->getRepository(Timelog::class)->find($timelogData['id']);
                $userEntity = $em->getRepository(User::class)->find($timelogData['userId']);
            } else {
                $logEntry = new Timelog();
                $timelogData['id'] = \Ramsey\Uuid\Uuid::uuid4();
            }
            $logEntry->setUser($userEntity);
            $logEntry->populateFromDto(new \Gfinance\Task\Model\TimeLog(...[
                'id' => $timelogData['id'],
                'timeLogged' => $timelogData['timeLogged'],
                'comment' => $timelogData['comment'],
                'taskId' => $task->getId()
            ]));
            $logEntry->setTags($tags);
            $em->persist($logEntry);
            $timelog[] = $logEntry;
        }
        return $timelog;
    }
}