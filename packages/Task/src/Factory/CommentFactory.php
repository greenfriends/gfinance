<?php

namespace Gfinance\Task\Factory;


use Gfinance\Task\Entity\Comment;
use Skeletor\User\Model\UserFactory;

class CommentFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $comment = $em->getRepository(Comment::class)->find($data['id']);
        $comment->populateFromDto(new \Gfinance\Task\Model\Comment(...$data));

        return $comment->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $comment = new Comment();
        $comment->populateFromDto(new \Gfinance\Task\Model\Comment(...$data));
        $em->persist($comment);

        return $comment->getId();
    }

    public static function make($data, $em)
    {
        $data['user'] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['user']), $em);
//        $data['taskId'] = $data['task']->getId();
        unset($data['userId']);
//        unset($data['task']);
//        unset($data['task_id']);

        return new \Gfinance\Task\Model\Comment(...$data);
    }
}