<?php

namespace Gfinance\Task\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use Gfinance\Tag\Factory\TagFactory;
use Gfinance\Task\Entity\Timelog;
use Skeletor\User\Model\UserFactory;

class TimelogFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $timelog = $em->getRepository(Timelog::class)->find($data['id']);
        $timelog->populateFromDto(new \Gfinance\Task\Model\TimeLog(...$data));

        return $timelog->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $timelog = new Timelog();
        $timelog->populateFromDto(new \Gfinance\Task\Model\TimeLog(...$data));
        $em->persist($timelog);

        return $timelog->getId();
    }

    public static function make($data, $em)
    {
        $data['user'] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['user']), $em);
        $tags = new ArrayCollection();
        foreach ($data['tags'] as $tagEntity) {
            $tags[] = TagFactory::make($em->getUnitOfWork()->getOriginalEntityData($tagEntity), $em);
        }
        $data['tags'] = $tags;
        unset($data['userId']);
        unset($data['task']);

        return new \Gfinance\Task\Model\TimeLog(...$data);
    }
}