<?php
namespace Gfinance\Task\Mapper;

use Gfinance\Timelog\Model\Task\Status;
use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\PDOWrite;

class Task extends MysqlCrudMapper
{
    const ANALYTICS_TYPE_MONTHLY = '%Y/%m';
    const ANALYTICS_TYPE_DAILY = '%d/%m/%Y';

    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'task', 'id');
    }

    public function fetchAnalytics($params = array(), $assignedProjects = false, $type = self::ANALYTICS_TYPE_MONTHLY)
    {
        $assignedUsersLogFilter = "";
        if (isset($params['assignedUsers'])) {
            $assignedUsersLogFilter = " AND userId IN ({$params['assignedUsers']})";
        }
        $timeLogSum = " SUM(timeLogged) / (3600) as totalLogged";
        $sql = "SELECT userId, DATE_FORMAT(timeLog.createdAt, '{$type}') as period, {$timeLogSum} 
        FROM `timeLog` JOIN task USING(taskId) WHERE 1=1 {$assignedUsersLogFilter}";
        if (isset($params['dateFrom'])) {
            $sql .= " AND (timeLog.createdAt >= '{$params['dateFrom']} 00:00:00') ";
            unset($params['dateFrom']);
        }
        if (isset($params['dateTo'])) {
            $sql .= " AND (timeLog.createdAt <= '{$params['dateTo']} 23:59:59') ";
            unset($params['dateTo']);
        }
        if (isset($params['status'])) {
            $sql .= " AND `status` IN ({$params['status']}) ";
            if (!in_array(Status::STATUS_ARCHIVED, explode(',', $params['status']))) {
//                $sql .= " AND `status` NOT IN (" . Status::STATUS_ARCHIVED . ") ";
            }
            unset($params['status']);
        } else {
//            $sql .= " AND `status` NOT IN (" . Status::STATUS_ARCHIVED . ") ";
        }
        if (isset($params['assignedUsers'])) {
            $sql .= " AND ( ";
            foreach (explode(',', $params['assignedUsers']) as $key => $userId) {
                if ($key > 0) {
                    $sql .= " OR ";
                }
                $sql .= " assignedUsers LIKE '%{$userId}%' ";
            }
            $sql .= " ) ";
            unset($params['assignedUsers']);
        }
        foreach ($params as $name => $value) {
            $sql .= " AND `{$name}` = '{$value}' ";
        }
        if ($assignedProjects !== false) {
            $sql .= " AND projectId IN ({$assignedProjects}) ";
        }
        $sql .= " GROUP BY userId, period ORDER BY period ASC";

//        echo $sql;
//        die();

        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function fetchWithTimeLog($params = array(), int $limit = null, $order = null, $assignedProjects = false)
    {
        $assignedUsersLogFilter = "";
        if (isset($params['assignedUsers'])) {
            $assignedUsersLogFilter = " AND userId IN ({$params['assignedUsers']})";
        }
        $timeLogSum = "(SELECT SUM(timeLogged) / (3600) FROM timeLog WHERE taskId = task.taskId {$assignedUsersLogFilter}) as totalLogged";
        $sql = "SELECT *, {$timeLogSum} FROM `{$this->tableName}` WHERE 1=1 ";
        if (isset($params['dateFrom'])) {
            $sql .= " AND (`createdAt` >= '{$params['dateFrom']} 00:00:00' OR `updatedAt` >= '{$params['dateFrom']} 00:00:00') ";
            unset($params['dateFrom']);
        }
        if (isset($params['dateTo'])) {
            $sql .= " AND (`createdAt` <= '{$params['dateTo']} 23:59:59' OR `updatedAt` <= '{$params['dateTo']} 00:00:00') ";
            unset($params['dateTo']);
        }
        if (isset($params['billable'])) {
            $sql .= " AND (`invoiceId` is NULL) ";
            unset($params['billable']);
        }
        if (isset($params['status'])) {
            $sql .= " AND `status` IN ({$params['status']}) ";
            if (!in_array(Status::STATUS_ARCHIVED, explode(',', $params['status']))) {
                $sql .= " AND `status` NOT IN (" . Status::STATUS_ARCHIVED . ") ";
            }
            unset($params['status']);
        } else {
            $sql .= " AND `status` NOT IN (" . Status::STATUS_ARCHIVED . ") ";
        }
        if (isset($params['assignedUsers'])) {
            $sql .= " AND ( ";
            foreach (explode(',', $params['assignedUsers']) as $key => $userId) {
                if ($key > 0) {
                    $sql .= " OR ";
                }
                $sql .= " assignedUsers LIKE '%{$userId}%' ";
            }
            $sql .= " ) ";
            unset($params['assignedUsers']);
        }
        foreach ($params as $name => $value) {
            $sql .= " AND `{$name}` = '{$value}' ";
        }
        if ($assignedProjects !== false) {
            $sql .= " AND projectId IN ({$assignedProjects}) ";
        }
        if ($order) {
            $sql .= " ORDER BY {$order['orderBy']} {$order['dir']} ";
        }
        if (isset($limit)) {
            $sql .= " LIMIT " . $limit;
        }
//        echo $sql;
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getByIdWithTimeLog($id)
    {
        $timeLogSum = "(SELECT SUM(timeLogged) / 3600 FROM timeLog WHERE taskId = task.id) as totalLogged";
        $sql = "SELECT *, {$timeLogSum} FROM `{$this->tableName}` WHERE taskId = " . $id;

        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC)[0];
    }
}