<?php
namespace Gfinance\Task\Filter;

use Laminas\Filter\ToInt;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Core\Filter\FilterInterface;
use Skeletor\Core\Service\BlocksParser;
use Skeletor\Core\Validator\ValidatorException;
use Skeletor\User\Service\Session;
use Volnix\CSRF\CSRF;

class Task implements FilterInterface
{
    /**
     * @var \Gfinance\Task\Validator\Task
     */
    private $validator;

    public function __construct(
        \Gfinance\Task\Validator\Task $validator, private Session $userSession, private BlocksParser $blocksParser
    ) {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter($postData) : array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $description = '';
        if (isset($postData['blocks'])) {
            $description = json_encode($this->blocksParser->parse((array) $postData['blocks']));
        }
        $data = [
            'id' => (isset($postData['id'])) ? $postData['id'] : null,
            'name' => $postData['name'],
            'projectId' => $postData['projectId'],
            'feature' => $postData['feature'] ?? null,
            'userId' => $this->userSession->getLoggedInUserId(),
            'status' => $int->filter($postData['status']),
            'createdBy' => $postData['createdBy'],
            'priority' => $int->filter($postData['priority']),
            'assignedUsers' => $postData['assignedUsers'],
            'description' => $description,
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        $comment = [];
        foreach ($postData['comment']['id'] as $key => $id) {
            if (strlen($postData['comment']['body'][$key]) === 0) {
                continue;
            }
            $comment[] = [
                'id' => $id,
                'body' => $postData['comment']['body'][$key],
                'userId' => $postData['comment']['userId'][$key]
            ];
        }
        $data['comment'] = $comment;
        $timelog = [];
        foreach ($postData['timelog']['id'] as $key => $id) {
            if (strlen($postData['timelog']['comment'][$key]) === 0) {
                continue;
            }
            $timelog[] = [
                'id' => $id,
                'tags' => $postData['timelog']['tags'] ?? [],
                'comment' => $postData['timelog']['comment'][$key],
                'userId' => $postData['timelog']['userId'][$key],
                'timeLogged' => $postData['timelog']['timeLogged'][$key] * 3600
            ];
        }
        $data['timelog'] = $timelog;
        if (!$this->validator->isValid($data)) {
            $e = new ValidatorException();
            $e->data = $data;
            throw $e;
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }
}