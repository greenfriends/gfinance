<?php

namespace Gfinance\Task\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Skeletor\Core\Entity\Timestampable;
use Gfinance\Task\Model\Comment as DtoModel;
use Skeletor\User\Entity\User;

#[ORM\Entity]
#[ORM\Table(name: 'comment')]
class Comment
{
    use Timestampable;

    #[ORM\Column(type: Types::TEXT)]
    private string $body;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'userId', referencedColumnName: 'id', unique: false)]
    private User $user;

    public function populateFromDto(DtoModel $dto)
    {
        if ($dto->getId()) {
            $this->id = $dto->getId();
        }
        $this->body = $dto->getBody();

        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getId()
    {
        return $this->id;
    }
}