<?php

namespace Gfinance\Task\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gfinance\Tag\Entity\Tag;
use Skeletor\Core\Entity\Timestampable;
use Gfinance\Task\Model\Timelog as DtoModel;
use Skeletor\User\Entity\User;

#[ORM\Entity]
#[ORM\Table(name: 'timelog')]
class Timelog
{
    use Timestampable;

    #[ORM\Column(type: Types::TEXT)]
    private string $comment;

    #[ORM\Column(type: Types::INTEGER)]
    private int $timeLogged;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'userId', referencedColumnName: 'id', unique: false)]
    private User $user;

    #[ORM\ManyToMany(targetEntity: Tag::class, fetch: 'EAGER', orphanRemoval: false, cascade: ['persist'])]
    private Collection $tags;

    public function populateFromDto(DtoModel $dto)
    {
        if ($dto->getId()) {
            $this->id = $dto->getId();
        }
        $this->comment = $dto->getComment();
        $this->timeLogged = $dto->getTimeLogged();

        return $this;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getId()
    {
        return $this->id;
    }
}