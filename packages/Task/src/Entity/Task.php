<?php
namespace Gfinance\Task\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gfinance\Client\Entity\Client;
use Gfinance\Project\Entity\Feature;
use Gfinance\Project\Entity\Project;
use Gfinance\Tag\Entity\Tag;
use Skeletor\Core\Entity\Timestampable;
use Gfinance\Task\Model\Task as DtoModel;
use Skeletor\User\Entity\User;

#[ORM\Entity]
#[ORM\Table(name: 'task')]
class Task
{
    use Timestampable;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $name;

    #[ORM\Column(type: Types::STRING, length: 16, unique: true)]
    private string $code;

    #[ORM\Column(type: Types::TEXT)]
    private string $description;

    #[ORM\Column(type: Types::INTEGER, length: 1)]
    private int $priority;

    #[ORM\Column(type: Types::INTEGER, length: 1)]
    private int $status;

    #[ORM\ManyToOne(targetEntity: Client::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'clientId', referencedColumnName: 'id', unique: false)]
    private Client $client;

    #[ORM\ManyToOne(targetEntity: Project::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'projectId', referencedColumnName: 'id', unique: false)]
    private Project $project;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'createdBy', referencedColumnName: 'id', unique: false)]
    private User $createdBy;

    #[ORM\ManyToOne(targetEntity: Feature::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'featureId', referencedColumnName: 'id', unique: false, nullable: true)]
    private ?Feature $feature;

    #[ORM\ManyToMany(targetEntity: User::class, fetch: 'EAGER')]
    private Collection $assignedUsers;

    #[ORM\ManyToMany(targetEntity: Comment::class, fetch: 'EAGER', orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $comments;

    #[ORM\ManyToMany(targetEntity: Timelog::class, fetch: 'EAGER', orphanRemoval: true, cascade: ['persist', 'remove'])]
    private Collection $timelog;

    public function populateFromDto(DtoModel $dto)
    {
        if ($dto->getId()) {
            $this->id = $dto->getId();
        }
        $this->name = $dto->getName();
        $this->code = $dto->getCode();
        $this->description = $dto->getDescription();
        $this->status = $dto->getStatus();
        $this->priority = $dto->getPriority();

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getDescription()
    {
        return $this->description;
    }

    public function setComments(Collection $comments)
    {
        $this->comments = $comments;
    }

    public function setTimelog(Collection $timelog)
    {
        $this->timelog = $timelog;
    }

    public function setAssignedUsers($assignedUsers)
    {
        $this->assignedUsers = $assignedUsers;
    }

    public function addAssignedUser(User $assignedUser)
    {
        $this->assignedUsers[] = $assignedUser;
    }

    public function setFeature($feature)
    {
        $this->feature = $feature;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }
}