<?php
namespace Gfinance\Api\Controller;

use Gfinance\Project\Service\Project;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\User\Service\User;

class ProjectController extends BaseApiController
{
    /**
     * @param Project $project
     * @param Config $config
     * @param User $userService
     * @param Logger $logger
     * @param Session $session
     */
    public function __construct(
        private Project $project, Config $config,
        private User $userService, private Logger $logger, Session $session
    ) {
        parent::__construct($config, $session);
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        try {
            $this->project->create($data);
            $message = 'Project successfully created.';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return $this->respond(['message' => $message]);
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        //@TODO validate data
        try {
            $this->project->update($data);
            $message = 'Project updated.';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return $this->respond(['message' => $message]);
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $projects = [];
        foreach ($this->project->getEntities() as $project) {
            $data = $project->toArray();
            $data['id'] = $project->getId();
            $projects[] = $data;
        }

        return $this->respond([
            'projects' => $projects
        ]);
    }

    /**
     * @return Response
     */
    public function delete(): Response
    {
        //@TODO is there a need to delete via api ?
        echo 'disabled';
        exit();
        try {
            $this->project->delete($this->getRequest()->getAttribute('id'));
            $message = 'Project successfully deleted';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return $this->respond(['message' => $message]);
    }
}