<?php

namespace Gfinance\Api\Controller;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest as Request;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Firebase\JWT\JWT;

class ApiController
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->response = new Response();
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable|null $next
     * @return mixed
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response, callable $next = null)
    {
        if (!$request->getAttribute('action')) {
            throw new \Exception('Action attribute is not set.');
        }
        $this->request = $request;
        $this->response = $response;
        if (!method_exists($this, $request->getAttribute('action'))) {
            throw new \Exception(sprintf('Action %s does not exist', $request->getAttribute('action')));
        }
        $checkJwt = $this->checkJwt($request, $response);
        if ($checkJwt) {
            return $checkJwt;
        }
        $response = $this->{$request->getAttribute('action')}();

        return $response;
    }

    private function checkJwt(Request $request, Response $response)
    {
        $auth = explode(' ', $request->getHeader('Authorization')[0]);
        if ($auth[0] !== 'Bearer') {
            $response = $response->withStatus(401);
            $response->getBody()->write(json_encode([
                "message" => "Access denied.",
                "error" => "No Bearer Token Found."
            ]));
            $response->getBody()->rewind();
        }
        $jwt = $auth[1];
        $secret_key = "someSecretKey";

        try {
            $decoded = JWT::decode($jwt, $secret_key, array('HS256'));
        } catch (\Exception $e) {
            $response = $response->withStatus(401);
            $response->getBody()->write(json_encode([
                "message" => "Access denied.",
                "error" => $e->getMessage()
            ]));
            $response->getBody()->rewind();

            return $response;
        }
    }

    /**
     * Return response to client.
     *
     * @param $template
     * @param array $data
     *
     * @return Response
     */
    public function respond($data = []) : Response
    {
        try {
            $this->response->getBody()->write(json_encode($data));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $this->response->getBody()->rewind();

        return $this->response;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Redirect client to given uri.
     *
     * @param $url
     *
     * @return Response
     */
    public function redirect($url): Response
    {
        if (!strstr($url, 'http')) {
            $url = $this->config->offsetGet('baseUrl') . $url;
        }

        return $this->response->withStatus(302)->withHeader('Location', $url);
    }
}