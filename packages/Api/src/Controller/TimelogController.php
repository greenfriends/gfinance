<?php
namespace Gfinance\Api\Controller;

use Gfinance\Task\Service\Timelog;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\User\Service\User;

class TimelogController extends BaseApiController
{
    const DAY = 60*60*8;
    const HOUR = 60*60;
    const MINUTE = 60;

    /**
     * @var Timelog
     */
    private $timeLogService;

    /**
     * @var User
     */
    private $userService;

    private $logger;

    /**
     * TimelogController constructor.
     * @param Timelog $timeLogRepo
     * @param Config $config
     * @param User $userService
     * @param Logger $logger
     */
    public function __construct(
        Timelog $timeLogService, Config $config, User $userService, Logger $logger, Session $session
    ) {
        parent::__construct($config, $session);

        $this->timeLogService = $timeLogService;
        $this->userService = $userService;
        $this->logger = $logger;
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        $data['timeLogged'] = $this->parseTime($data['timeLogged']);
        $trigger = null;
        if (isset($data['trigger'])) {
            $trigger = $data['trigger'];
            unset($data['trigger']);
        }
        try {
            $this->timeLogService->create($data, $trigger);
            $message = 'Timelog successfully created';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return $this->respond(['message' => $message]);
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        $data['timeLogged'] = $this->parseTime($data['timeLogged']);
        $trigger = null;
        if (isset($data['trigger'])) {
            $trigger = $data['trigger'];
            unset($data['trigger']);
        }
        //@TODO validate data
        try {
            $this->timeLogService->update($data, $trigger);
            $message = 'Timelog updated.';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return $this->respond(['message' => $message]);
    }

    private function parseTime($time)
    {
        $time = strtolower(str_replace(' ', '', $time));
        $days = explode('d', $time);
        $loggedTime = 0;
        if (isset($days[1])) {
            $time = $days[1];
            $loggedTime += $days[0] * self::DAY;
        }
        $hours = explode('h', $time);
        if (isset($hours[1])) {
            $time = $hours[1];
            if ($hours[0] > 60) {
                throw new \Exception('Invalid hour format');
            }
            $loggedTime += $hours[0] * self::HOUR;
        }
        $minutes = explode('m', $time);
        if (isset($minutes[1])) {
            $time = $minutes[1];
            if ($minutes[0] > 60) {
                throw new \Exception('Invalid minute format');
            }
            $loggedTime += $minutes[0] * self::MINUTE;
        }
        $seconds = explode('s', $time);
        if (isset($seconds[1])) {
            if ($seconds[0] > 60) {
                throw new \Exception('Invalid seconds format');
            }
            $loggedTime += $seconds[0];
        }
        return $loggedTime;
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        return $this->respond(['timeLogs' => $this->timeLogService->getAll()]);
    }

    public function delete(): Response
    {
        try {
            $this->timeLogService->deleteTimeLog((int) $this->getRequest()->getAttribute('id'));
            $message = 'Time log successfully deleted';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return $this->respond(['message' => $message]);
    }
//
//    public function ajaxHandle(): Response
//    {
//        $data = $this->getRequest()->getParsedBody();
//        $dtRequest = new DataTablesRequest($data);
//        $response = $this->getResponse()->withAddedHeader('Content-Type', 'application/json');
//        $response->getBody()->write($this->articleService->getDataTableResponse($dtRequest)->getData());
//
//        return $response;
//    }

}