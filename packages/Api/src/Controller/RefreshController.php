<?php
namespace Gfinance\Api\Controller;

use GuzzleHttp\Psr7\Query;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Skeletor\User\Service\User;
use Psr\Log\LoggerInterface as Logger;
use Gfinance\Timelog\Service\Jwt;

class RefreshController extends BaseApiController
{
    private $logger;

    private $jwt;

    /**
     * @param Jwt $jwtService
     * @param Session $session
     * @param Config $config
     * @param User $userService
     * @param Logger $logger
     */
    public function __construct(
        Jwt $jwtService, Session $session, Config $config, User $userService, Logger $logger
    ) {
        parent::__construct($config, $session);

        $this->jwt = $jwtService;
        $this->logger = $logger;
    }

    public function __invoke(ServerRequest $request, Response $response, callable $next = null): Response
    {
        $data = $request->getBody()->getContents();

        return $this->refresh(Query::parse($data));
    }

    public function refresh($data): Response
    {
        try {
            if (!isset($data['refreshToken'])) {
                throw new \Exception('Wrong params provided.');
            }
            $accessToken = $this->jwt->refreshAccessToken($data['refreshToken']);
            $message = "Refreshed token";
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $accessToken = '';
        }

        $this->getResponse()->getBody()->write(json_encode([
            'message' => $message,
            'accessToken' => $accessToken,
        ]));

        return $this->getResponse();
    }
}