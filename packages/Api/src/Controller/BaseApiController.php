<?php

namespace Gfinance\Api\Controller;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest as Request;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;

class BaseApiController
{
    protected Response $response;

    protected Request $request;

    public string $protectedPath;

    public function __construct(private Config $config, private Session $session)
    {
        $this->response = new Response();
        $this->protectedPath = $config->offsetGet('adminPath');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable|null $next
     * @return mixed
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response, callable $next = null)
    {
        if (!$request->getAttribute('action')) {
            throw new \Exception('Action attribute is not set.');
        }
        $this->request = $request;
        $this->response = $response;

        if (!method_exists($this, $request->getAttribute('action'))) {
            throw new \Exception(sprintf('Method %s does not exist', $request->getAttribute('action')));
        }
        $checkJwt = $this->checkJwt($request, $response);
        if ($checkJwt) {
            return $checkJwt;
        }
        $this->response = $this->{$request->getAttribute('action')}();

        return $this->response;
    }

    /**
     * Return response to client.
     *
     * @param array $data
     *
     * @return Response
     */
    public function respond(array $data = []) : Response
    {
        $this->response->getBody()->write(json_encode($data));
        $this->response->getBody()->rewind();

        return $this->response;
    }

    private function checkJwt(Request $request, Response $response)
    {
        $auth = explode(' ', $request->getHeader('Authorization')[0]);
        if ($auth[0] !== 'Bearer') {
            $response = $response->withStatus(401);
            $response->getBody()->write(json_encode([
                "message" => "Access denied.",
                "error" => "No Bearer Token Found."
            ]));
            $response->getBody()->rewind();
        }
        $jwt = $auth[1];
        $key = new Key('someSecretKey', 'HS256');
        try {
            $decoded = JWT::decode($jwt, $key);
        } catch (\Exception $e) {
            $response = $response->withStatus(401);
            $response->getBody()->write(json_encode([
                "message" => "Access denied.",
                "error" => $e->getMessage()
            ]));
            $response->getBody()->rewind();

            return $response;
        }
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->response;
    }

    /**
     * @return Session
     */
    public function getSession() : Session
    {
        return $this->session;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Redirect client to given uri.
     *
     * @param $url
     *
     * @return Response
     */
    public function redirect($url): Response
    {
        if (!strlen($this->protectedPath)) {
            $url = str_replace('admin/', '', $url);
        } else {
            $url = str_replace('admin/', sprintf('%s/', $this->protectedPath), $url);
        }
        if (!strstr($url, 'http')) {
            $url = $this->config->offsetGet('baseUrl') . $url;
        }

        return $this->response->withStatus(302)->withHeader('Location', $url);
    }
}