<?php
namespace Gfinance\Api\Controller;

use GuzzleHttp\Psr7\Response;
use Skeletor\User\Service\User as UserService;
use Skeletor\User\Validator\User;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;

class UserController extends BaseApiController
{
    /**
     * @param UserService $userService
     * @param Session $session
     * @param Config $config
     * @param User $validator
     */
    public function __construct(
        private UserService $userService, Session $session, Config $config
    ) {
        parent::__construct($config, $session);
    }

    /**
     * User list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $users = [];
        foreach ($this->userService->getEntities() as $user) {
            $userData = $user->toArray();
            $userData['id'] = $user->getId();
            $users[] = $userData;
        }

        return $this->respond(['users' => $users]);
    }
}