<?php
declare(strict_types=1);
namespace Gfinance\Api\Controller;

use GuzzleHttp\Psr7\Query;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use League\Plates\Engine;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Core\Controller\Controller;
use Psr\Log\LoggerInterface as Logger;
use Gfinance\Timelog\Service\Jwt;

class LoginController extends Controller
{

    /**
     * @var User
     */
    private $userService;

    private $logger;

    private $jwt;

    /**
     * @param Jwt $jwtService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param User $userService
     * @param Logger $logger
     */
    public function __construct(
        Jwt $jwtService, Session $session, Config $config, Flash $flash, Engine $template,
        User $userService, Logger $logger
    ) {
        parent::__construct($template, $config, $session, $flash);

        $this->jwt = $jwtService;
        $this->userService = $userService;
        $this->logger = $logger;
    }

    public function __invoke(ServerRequest $request, Response $response, callable $next = null): Response
    {
        $data = $request->getBody()->getContents();

        return $this->authenticate(Query::parse($data));
    }

    public function authenticate($data): Response
    {
        try {
            if (!isset($data['email']) || !isset($data['password'])) {
                throw new \Exception('Wrong params provided.');
            }
            $email = $data['email'];
            $password = $data['password'];
            $this->userService->login([
                'email' => $email,
                'password' => $password,
            ]);
            $user = $this->userService->getRepository()->getByEmail($email);
            $message = $this->userService->getMessages()[0];
//            $jwt = $this->jwt->authenticate($user);
            $accessToken = $this->jwt->issueAccessToken($user);
            $refreshToken = $this->jwt->issueRefreshToken($user);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $refreshToken = '';
            $accessToken = '';
//            $jwt = '';
        }

        $this->getResponse()->getBody()->write(json_encode([
            'message' => $message,
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken,
//            'jwt' => $jwt,
        ]));

        return $this->getResponse();
    }
}