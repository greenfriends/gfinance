<?php
declare(strict_types=1);
namespace Gfinance\Api\Controller;

use Gfinance\Project\Model\Project;
use Gfinance\Project\Repository\ProjectRepository;
use Gfinance\Task\Service\Task as TaskService;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\User\Service\User;

class TaskController extends BaseApiController
{
    /**
     * @param TaskService $taskService
     * @param User $userService
     * @param Logger $logger
     * @param Config $config
     * @param ProjectRepository $projectRepository
     * @param Session $session
     */
    public function __construct(
        private TaskService $taskService, private Logger $logger, Config $config,
        private ProjectRepository $projectRepository, Session $session
    ) {
        parent::__construct($config, $session);
    }

    public function create(): Response
    {
        $taskId = null;
        try {
            $data = $this->getRequest()->getParsedBody();
            $attachments = []; // @TODO
            if (isset($this->getRequest()->getUploadedFiles()['attachments'])) {
                $attachments = $this->getRequest()->getUploadedFiles()['attachments'];
            }
            $trigger = null;
            if (isset($data['trigger'])) {
                $trigger = $data['trigger'];
                unset($data['trigger']);
            }
            $task = $this->taskService->create($data);
            $taskId = $task->getId();
            $message = 'Task successfully created.';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return $this->respond(['message' => $message, 'id' => $taskId]);
    }

    public function update(): Response
    {
        $files = $this->getRequest()->getUploadedFiles()['attachments'];
        $data = $this->getRequest()->getParsedBody();
        $trigger = null;
        if (isset($data['trigger'])) {
            $trigger = $data['trigger'];
            unset($data['trigger']);
        }
        //@TODO validate data, attachments
        try {
            $this->taskService->update($data);
            $message = 'Task successfully updated.';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return $this->respond(['message' => $message]);
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $params = $this->getRequest()->getQueryParams();
        $projects = $this->projectRepository->fetchAll(['type' => Project::TYPE_OUT]);
        $ids = [];
        foreach ($projects as $project) {
            $ids[] = $project->getId();
        }

        $tasks = [];
        foreach ($this->taskService->getEntities($params, null, ['createdAt' => 'DESC']) as $task) {
            $tasks[] = $task->toArray();
        }

        return $this->respond([
            'tasks' => $tasks
        ]);
    }

    /**
     * @TODO make sure there are no relations before deleting
     *
     * @return Response
     */
    public function delete(): Response
    {
        try {
            $taskId = $this->getRequest()->getAttribute('id');
            if (!$taskId) {
                $message = 'Invalid taskId parameter provided.';
            } else {
                $message = 'Task successfully deleted';
                $this->taskService->delete($taskId);
            }
        } catch (\Exception $e) {
            $message = 'Task could not be deleted. ' . $e->getMessage();
        }

        return $this->respond(['message' => $message]);
    }

}