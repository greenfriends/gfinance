<?php
namespace Gfinance\Project\Service;

use Gfinance\Client\Service\Client;
use Gfinance\Project\Filter\Project as Filter;
use Gfinance\Project\Repository\ProjectRepository as Repository;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Core\TableView\Service\TableView;
use Skeletor\User\Service\Session;

class Project extends TableView
{
    const APPLY_TENANT_FILTER = false;

    /**
     * @param Repository $repo
     * @param Session $userSession
     * @param Logger $logger
     */
    public function __construct(
        Repository $repo, Session $userSession, Logger $logger, Filter $filter, private Client $client,
        private \GuzzleHttp\Client $httpClient
    ) {
        parent::__construct($repo, $userSession, $logger, $filter);
    }

    public function getDeployLog($code, $env)
    {
        $url = sprintf('http://dep.greenfriends.systems/hooks/getLog/?project=%s&env=%s', $code, $env);
        $response = $this->httpClient->send(new Request('get', $url));

        return $response->getBody()->getContents();
    }

    public function prepareEntities($entities)
    {
        $items = [];
        foreach ($entities as $project) {
            $items[] = [
                'columns' => [
                    'id' => $project->getId(),
                    'name' =>  [
                        'value' => $project->getName(),
                        'editColumn' => true,
                    ],
                    'code' => $project->getCode(),
                    'type' => $project->getType(),
                    'client' => $project->getClient()->getName(),
                    'isActive' => ($project->getIsActive()) ? 'Active':'Inactive',
                    'createdAt' => $project->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $project->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $project->getId(),
            ];
        }
        return $items;
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'code', 'label' => 'Code'],
            ['name' => 'type', 'label' => 'Type'],
            ['name' => 'client', 'label' => 'Client', 'filterData' => $this->client->getFilterData()],
            ['name' => 'isActive', 'label' => 'Active', 'filterData' => [0 => 'Inactive', 1 => 'Active']],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Created at'],
        ];

        return $columnDefinitions;
    }
}