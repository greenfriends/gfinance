<?php
namespace Gfinance\Project\Service;

use Gfinance\Project\Repository\FeatureRepository;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Core\TableView\Service\TableView;
use Skeletor\User\Service\Session;

class Feature extends TableView
{
    const APPLY_TENANT_FILTER = false;

    /**
     * Deploy constructor.
     * @param $deployRepo
     * @param $httpClient
     */
    public function __construct(
        FeatureRepository $repo, Session $userSession, Logger $logger, \Gfinance\Project\Filter\Feature $filter, private Project $project
    ) {
        parent::__construct($repo, $userSession, $logger, $filter);
    }

    public function prepareEntities($entities)
    {
        $items = [];
        foreach ($entities as $feature) {
            $items[] = [
                'columns' => [
                    'id' => $feature->getId(),
                    'name' =>  [
                        'value' => $feature->getName(),
                        'editColumn' => true,
                    ],
                    'project' => $feature->getProject()->getName(),
                    'createdAt' => $feature->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $feature->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $feature->getId(),
            ];
        }
        return $items;
    }

    public function compileTableColumns()
    {
        $projectFilter = [];
        foreach ($this->project->getEntities(['isActive' => 1], null, ['name' => 'asc']) as $project) {
            $projectFilter[$project->getId()] = $project->getName();
        }

        $columnDefinitions = [
//            ['name' => 'id', 'label' => '#'],
            ['name' => 'name', 'label' => 'Name'],
            ['name' => 'project', 'label' => 'Project', 'filterData' => $projectFilter],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
        ];

        return $columnDefinitions;
    }
}