<?php
namespace Gfinance\Project\Service;

use Gfinance\Project\Repository\DeployRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Core\TableView\Service\TableView;
use Skeletor\User\Service\Session;

class Deploy extends TableView
{
    const APPLY_TENANT_FILTER = false;

    /**
     * Deploy constructor.
     * @param $deployRepo
     * @param $httpClient
     */
    public function __construct(
        DeployRepository $repo, Session $userSession, Logger $logger, private Client $httpClient
    ) {
        parent::__construct($repo, $userSession, $logger);
    }

    public function getByProjectId($projectId)
    {
        try {
            $deploy = $this->repo->getById($projectId);
        } catch (\Exception $e) {
            $deploy = null;
        }
        return $deploy;
    }

    public function getDeployLog($code)
    {
        $url = 'http://dep.greenfriends.systems/hooks/getLog/?project=' . $code;
        $response = $this->httpClient->send(new Request('get', $url));

        return $response->getBody()->getContents();
    }

    public function fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter = null)
    {
        $data = $this->repo->fetchTableData($search, $filter, $offset, $limit, $order, $uncountableFilter);
        $items = [];
        foreach ($data['entities'] as $deploy) {
            $items[] = [
                'columns' => [
                    'id' => $deploy->getId(),
                    'projectCode' =>  [
                        'value' => $deploy->getProjectCode(),
                        'editColumn' => true,
                    ],
                    'branch' => $deploy->getBranch(),
                    'createdAt' => $deploy->getCreatedAt()->format('d.m.Y'),
                    'updatedAt' => $deploy->getCreatedAt()->format('d.m.Y'),
                ],
                'id' => $deploy->getId(),
            ];
        }
        return [
            'count' => $data['count'],
            'entities' => $items,
        ];
    }

    public function compileTableColumns()
    {
        $columnDefinitions = [
            ['name' => 'id', 'label' => '#'],
            ['name' => 'projectCode', 'label' => 'Code'],
            ['name' => 'branch', 'label' => 'Branch', 'filterData' => ['master' => 'master', 'develop' => 'develop']],
            ['name' => 'createdAt', 'label' => 'Created at'],
            ['name' => 'updatedAt', 'label' => 'Updated at'],
        ];

        return $columnDefinitions;
    }
}