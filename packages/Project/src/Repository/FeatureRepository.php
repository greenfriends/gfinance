<?php
namespace Gfinance\Project\Repository;

use Skeletor\Core\TableView\Repository\TableViewRepository;


class FeatureRepository extends TableViewRepository
{
    const ENTITY = \Gfinance\Project\Entity\Feature::class;
    const FACTORY = \Gfinance\Project\Factory\FeatureFactory::class;

    public function getSearchableColumns(): array
    {
        return ['name'];
    }

}
