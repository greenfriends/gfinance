<?php
namespace Gfinance\Project\Repository;

use Gfinance\Project\Model\Deploy as Model;
use Skeletor\TableView\Repository\TableViewRepository;

class DeployRepository extends TableViewRepository
{
    public function __construct(
        \Gfinance\Project\Mapper\Deploy $mapper, \DateTime $dt
    ) {
        parent::__construct($mapper, $dt);
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
//            $this->project->getById($data['projectId']),
            $data['projectId'],
            $data['id'],
            $data['projectCode'],
            $data['branch'],
            $data['env'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function getSearchableColumns(): array
    {
        return ['projectCode'];
    }

}