<?php
namespace Gfinance\Project\Repository;

use Skeletor\Core\TableView\Repository\TableViewRepository;


class ProjectRepository extends TableViewRepository
{
    const ENTITY = \Gfinance\Project\Entity\Project::class;
    const FACTORY = \Gfinance\Project\Factory\ProjectFactory::class;

    public function getAssignedUsers($projectId)
    {
        return $this->mapper->getAssignedUsers($projectId);
    }

    public function getOutProjects()
    {
        $items = [];
        foreach ($this->mapper->fetchAll(['type' => Project::TYPE_OUT]) as $data) {
            $items[] = $data['projectId'];
        }

        return $items;
    }

    public function getSearchableColumns(): array
    {
        return ['name', 'code'];
    }

    public function codeExists(string $code, $projectId): bool
    {
        $client = $this->entityManager->getRepository(static::ENTITY)->findOneBy(['code' => $code, 'id' => $projectId]);
        if ($client) {
            return true;
        }
        return false;
    }
}
