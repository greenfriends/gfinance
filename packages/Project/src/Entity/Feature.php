<?php
namespace Gfinance\Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gfinance\Task\Entity\Task;
use Skeletor\Core\Entity\Timestampable;
use Gfinance\Project\Model\Feature as DtoModel;

#[ORM\Entity]
#[ORM\Table(name: 'feature')]
class Feature
{
    use Timestampable;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $name;

    #[ORM\ManyToOne(targetEntity: Project::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'projectId', referencedColumnName: 'id', unique: false)]
    private Project $project;

    #[ORM\OneToMany(targetEntity: Task::class, fetch: 'LAZY', mappedBy: 'feature')]
    private Collection $tasks;

    public function __construct()
    {
        $this->assignedUsers = new ArrayCollection();
    }

    public function populateFromDto(DtoModel $dto)
    {
        if ($dto->getId()) {
            $this->id = $dto->getId();
        }
        $this->name = $dto->getName();

        return $this;
    }

    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    public function getId()
    {
        return $this->id;
    }
}