<?php
namespace Gfinance\Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gfinance\Client\Entity\Client;
use Skeletor\Core\Entity\Timestampable;
use Gfinance\Project\Model\Project as DtoModel;
use Skeletor\User\Entity\User;

#[ORM\Entity]
#[ORM\Table(name: 'project')]
class Project
{
    use Timestampable;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $name;

    #[ORM\Column(type: Types::STRING, length: 3)]
    private string $code;

    #[ORM\Column(type: Types::STRING, length: 4)]
    private string $type;

    #[ORM\Column(type: Types::JSON)]
    private string $deploy;

    #[ORM\Column(type: Types::INTEGER, length: 1)]
    private int $isActive;

    #[ORM\ManyToOne(targetEntity: Client::class, fetch: 'EAGER')]
    #[ORM\JoinColumn(name: 'clientId', referencedColumnName: 'id', unique: false)]
    private Client $client;

    #[ORM\ManyToMany(targetEntity: User::class, fetch: 'EAGER')]
    private Collection $assignedUsers;

    public function __construct()
    {
        $this->assignedUsers = new ArrayCollection();
    }

    public function populateFromDto(DtoModel $dto)
    {
        if ($dto->getId()) {
            $this->id = $dto->getId();
        }
        $this->name = $dto->getName();
        $this->code = $dto->getCode();
        $this->type = $dto->getType();
        $this->isActive = $dto->getIsActive();

        return $this;
    }

    public function setAssignedUsers($assignedUsers)
    {
        $this->assignedUsers = $assignedUsers;
    }

    public function addAssignedUser(User $assignedUser)
    {
        $this->assignedUsers[] = $assignedUser;
    }

    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setDeploy($deploy)
    {
        $this->deploy = $deploy;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getId()
    {
        return $this->id;
    }
}