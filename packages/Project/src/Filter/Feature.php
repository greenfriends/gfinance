<?php
namespace Gfinance\Project\Filter;

use Laminas\Filter\ToInt;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Core\Filter\FilterInterface;
use Gfinance\Project\Validator\Feature as Validator;
use Skeletor\Core\Validator\ValidatorException;
use Volnix\CSRF\CSRF;

class Feature implements FilterInterface
{

    public function __construct(private Validator $validator)
    {
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter($postData) : array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $data = [
            'id' => (isset($postData['id'])) ? $postData['id'] : null,
            'name' => $postData['name'],
            'projectId' => $postData['projectId'],
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            $e = new ValidatorException();
            $e->data = $data;
            throw $e;
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }
}