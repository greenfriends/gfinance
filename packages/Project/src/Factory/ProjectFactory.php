<?php
namespace Gfinance\Project\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use Gfinance\Client\Entity\Client;
use Gfinance\Client\Factory\ClientFactory;
use Gfinance\Project\Entity\Project;
use Gfinance\Project\Model\Deploy;
use Skeletor\User\Entity\User;
use Skeletor\User\Model\UserFactory;

class ProjectFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $project = $em->getRepository(Project::class)->find($data['id']);
        $client = $em->getRepository(Client::class)->find($data['clientId']);
        $users = new ArrayCollection();
        foreach ($data['assignedUsers'] as $userId) {
            $users[] = $em->getRepository(User::class)->find($userId);
        }
        $data['deploy'] = json_encode($data['deploy']);
        $project->setDeploy($data['deploy']);
        unset($data['deploy']);
        unset($data['clientId']);
        unset($data['assignedUsers']);
        $project->populateFromDto(new \Gfinance\Project\Model\Project(...$data));
        $project->setClient($client);
        $project->setAssignedUsers($users);

        return $project->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $project = new Project();
        $data['id'] = \Ramsey\Uuid\Uuid::uuid4();
        $client = $em->getRepository(Client::class)->find($data['clientId']);
        $users = new ArrayCollection();
        foreach ($data['assignedUsers'] as $userId) {
            $users[] = $em->getRepository(User::class)->find($userId);
        }
        $data['deploy'] = json_encode($data['deploy']);
        $project->setDeploy($data['deploy']);
        unset($data['deploy']);
        unset($data['assignedUsers']);
        unset($data['clientId']);
        $project->populateFromDto(new \Gfinance\Project\Model\Project(...$data));
        $project->setClient($client);
        $project->setAssignedUsers($users);
        $em->persist($project);

        return $project->getId();
    }

    public static function make($data, $em)
    {
        $data['client'] = ClientFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['client']), $em);
        $users = new ArrayCollection();
        foreach ($data['assignedUsers'] as $userEntity) {
            $users[] = UserFactory::make($em->getUnitOfWork()->getOriginalEntityData($userEntity), $em);
        }
        $data['assignedUsers'] = $users;
        unset($data['clientId']);
        $deployments = [];
        if ($data['deploy']) {
            foreach (json_decode($data['deploy']) as $env => $props) {
                if (!isset($props->projectCode)) {
                    continue;
                }
                $deployments[$env] = new Deploy(...[
                    'projectId' => $data['id'],
                    'projectCode' => $props->projectCode,
                    'env' => $env,
//                'branch' => ($env === 'production') ? 'master' : 'develop'
                    'branch' => $props->branch
                ]);
            }
        }
        $data['deploy'] = $deployments;
        return new \Gfinance\Project\Model\Project(...$data);
    }

    public static function makeDeploy()
    {

    }
}