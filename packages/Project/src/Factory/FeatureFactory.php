<?php
namespace Gfinance\Project\Factory;

use Doctrine\Common\Collections\ArrayCollection;
use Gfinance\Project\Entity\Feature;
use Gfinance\Project\Entity\Project;

class FeatureFactory
{
    public static function compileEntityForUpdate($data, $em)
    {
        $feature = $em->getRepository(Feature::class)->find($data['id']);
        $project = $em->getRepository(Project::class)->find($data['projectId']);
        unset($data['projectId']);
        $feature->populateFromDto(new \Gfinance\Project\Model\Feature(...$data));
        $feature->setProject($project);

        return $feature->getId();
    }

    public static function compileEntityForCreate($data, $em)
    {
        $feature = new Feature();
        $data['id'] = \Ramsey\Uuid\Uuid::uuid4();
        $project = $em->getRepository(Project::class)->find($data['projectId']);
        unset($data['projectId']);
        $feature->populateFromDto(new \Gfinance\Project\Model\Feature(...$data));
        $feature->setProject($project);
        $em->persist($feature);

        return $feature->getId();
    }

    public static function make($data, $em)
    {
        $data['project'] = ProjectFactory::make($em->getUnitOfWork()->getOriginalEntityData($data['project']), $em);
        unset($data['tasks']);
        unset($data['projectId']);

        return new \Gfinance\Project\Model\Feature(...$data);
    }
}