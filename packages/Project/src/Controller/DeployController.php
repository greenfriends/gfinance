<?php
namespace Gfinance\Project\Controller;

use Gfinance\Project\Service\Project;
use Gfinance\Project\Service\Deploy;
use Skeletor\User\Service\User;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Skeletor\Core\Controller\AjaxCrudController;
use Tamtamchik\SimpleFlash\Flash;

class DeployController extends AjaxCrudController
{
    const TITLE_VIEW = "View deploy";
    const TITLE_CREATE = "Create new deploy";
    const TITLE_UPDATE = "Edit deploy: ";
    const TITLE_UPDATE_SUCCESS = "deploy updated successfully.";
    const TITLE_CREATE_SUCCESS = "deploy created successfully.";
    const TITLE_DELETE_SUCCESS = "deploy deleted successfully.";
    const PATH = 'deploy';

    protected $tableViewConfig = ['writePermissions' => false, 'useModal' => false];

    /**
     * @param Deploy $deploy
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param User $userService
     */
    public function __construct(
        Deploy $deploy, Session $session, Config $config, Flash $flash, Engine $template, private Project $project,
        User $userService
//        , WatchlistRepository $watchlistRepo, Deploy $deploy
    ) {
        parent::__construct($deploy, $session, $config, $flash, $template);
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $deploy = null;
        $project = null;
        $deployLog = false;
        $url = false;
        if ($id) {
            $deploy = $this->service->getById($id);
            $project = $this->project->getById($deploy->getProjectId());
            $deployLog = $this->service->getDeployLog($deploy->getProjectCode());
            $this->setGlobalVariable('pageTitle', 'Edit deploy: ' . $id);
            $url = 'https://dep.greenfriends.systems/hooks/trigger/';
            $url = sprintf($url . '?project=%s&branch=%s&env=%s', $deploy->getProjectCode(), $deploy->getBranch(), $deploy->getEnv());
        }
        $this->formData['deploy'] = $deploy;
        $this->formData['project'] = $project;
        $this->formData['deployLog'] = $deployLog;
        $this->formData['triggerUrl'] = $url;

        return parent::form();
    }

}