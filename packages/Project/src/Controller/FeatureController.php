<?php
namespace Gfinance\Project\Controller;

use Gfinance\Project\Service\Feature;
use Gfinance\Project\Service\Project;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Skeletor\Core\Controller\AjaxCrudController;
use Tamtamchik\SimpleFlash\Flash;

class FeatureController extends AjaxCrudController
{
    const TITLE_VIEW = "View feature";
    const TITLE_CREATE = "Create new feature";
    const TITLE_UPDATE = "Edit feature: ";
    const TITLE_UPDATE_SUCCESS = "feature updated successfully.";
    const TITLE_CREATE_SUCCESS = "feature created successfully.";
    const TITLE_DELETE_SUCCESS = "feature deleted successfully.";
    const PATH = 'feature';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    /**
     * @param Project $project
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     */
    public function __construct(
        Feature $feature, Session $session, Config $config, Flash $flash, Engine $template, private Project $project
    ) {
        parent::__construct($feature, $session, $config, $flash, $template);
    }

    public function form(): Response
    {
        $this->formData['projects'] = $this->project->getEntities(['isActive' => 1], null, ['name' => 'asc']);

        return parent::form();
    }

}