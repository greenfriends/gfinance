<?php
namespace Gfinance\Project\Controller;

use Gfinance\Client\Service\Client;
use Gfinance\Project\Service\Deploy;
use Gfinance\Project\Service\Project;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Skeletor\Core\Controller\AjaxCrudController;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;

class ProjectController extends AjaxCrudController
{
    const TITLE_VIEW = "View project";
    const TITLE_CREATE = "Create new project";
    const TITLE_UPDATE = "Edit project: ";
    const TITLE_UPDATE_SUCCESS = "project updated successfully.";
    const TITLE_CREATE_SUCCESS = "project created successfully.";
    const TITLE_DELETE_SUCCESS = "project deleted successfully.";
    const PATH = 'project';

    protected $tableViewConfig = ['writePermissions' => true, 'useModal' => true];

    /**
     * @param Project $project
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     */
    public function __construct(
        Project $project, Session $session, Config $config, Flash $flash, Engine $template, private Client $client,
        private User $user
    ) {
        parent::__construct($project, $session, $config, $flash, $template);
    }

    public function form(): Response
    {
        $this->formData['clients'] = $this->client->getEntities();
        $this->formData['users'] = $this->user->getEntities(['isActive' => 1]);
        $this->formData['deployLog']['production'] = null;
        $this->formData['deployLog']['staging'] = null;
        $this->formData['triggerUrl']['production'] = null;
        $this->formData['triggerUrl']['staging'] = null;
        $id = $this->getRequest()->getAttribute('id');
        $url = 'https://dep.greenfriends.systems/hooks/trigger/';
        if ($id) {
            $model = $this->service->getById($id);
            if (is_object($model->getDeploy()['production'])) {
                $deploy = $model->getDeploy()['production'];
                $this->formData['deployLog']['production'] = $this->service->getDeployLog($deploy->getProjectCode(), $deploy->getEnv());
                $this->formData['triggerUrl']['production'] = $url . sprintf('?project=%s&branch=%s&env=%s', $deploy->getProjectCode(), $deploy->getBranch(), $deploy->getEnv());
            }
            if (is_object($model->getDeploy()['staging'])) {
                $deploy = $model->getDeploy()['staging'];
                $this->formData['deployLog']['staging'] = $this->service->getDeployLog($deploy->getProjectCode(), $deploy->getEnv());
                $this->formData['triggerUrl']['staging'] = $url . sprintf('?project=%s&branch=%s&env=%s', $deploy->getProjectCode(), $deploy->getBranch(), $deploy->getEnv());
            }
        }

        return parent::form();
    }

}