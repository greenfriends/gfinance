<?php
namespace Gfinance\Project\Validator;

use Gfinance\Project\Repository\ProjectRepository;
use Volnix\CSRF\CSRF;

class Project
{
    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * @param CSRF $csrf
     */
    public function __construct(CSRF $csrf, private ProjectRepository $repo)
    {
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;
        if (strlen($data['name']) < 1) {
            $this->messages['name'][] = 'Name must be entered.';
            $valid = false;
        }

        if (!$data['id'] && $this->repo->codeExists($data['code'], $data['clientId'])) {
            $this->messages['name'][] = 'Code already exists in system and must be unique per client.';
            $valid = false;
        }

        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Invalid form key.';
            $valid = false;
        }

        return $valid;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}