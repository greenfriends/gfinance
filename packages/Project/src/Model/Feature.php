<?php

namespace Gfinance\Project\Model;

use Skeletor\Core\Model\Model;

class Feature extends Model
{
    private $id;
    private $project;
    private $name;

    /**
     * @param $id
     * @param Project|null $project
     * @param $createdAt
     * @param $updatedAt
     */
    public function __construct($id, $name, ?Project $project = null, $createdAt = null, $updatedAt = null)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->name = $name;
        $this->project = $project;
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

}