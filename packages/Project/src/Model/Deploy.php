<?php

namespace Gfinance\Project\Model;

use Gfinance\Client\Model\Client;
use Skeletor\Core\Model\Model;

class Deploy extends Model
{
    private $projectId;

    private $projectCode;

    private $branch;

    private $env;

    /**
     * Project constructor.
     * @param $projectId
     * @param $name
     * @param Client $client
     */
    public function __construct($projectId, $projectCode, $branch, $env, $createdAt = null, $updatedAt = null)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->projectId = $projectId;
        $this->projectCode = $projectCode;
        $this->branch = $branch;
        $this->env = $env;
    }

    /**
     * @return mixed
     */
    public function getEnv()
    {
        return $this->env;
    }

    public function getId()
    {
        return 0;
    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @return mixed
     */
    public function getProjectCode()
    {
        return $this->projectCode;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

}