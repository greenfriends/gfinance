<?php
namespace Gfinance\Project\Model;

use Doctrine\Common\Collections\Collection;
use Gfinance\Client\Model\Client;
use Skeletor\Core\Model\Model;

class Project extends Model
{
    const TYPE_IN = 'I';
    const TYPE_OUT = 'O';

    private $id;

    private $name;

    private $code;

    private $type;

    /**
     * @var Client
     */
    private $client;

    private $deploy;

    private $isActive;

    /**
     * Project constructor.
     * @param $projectId
     * @param $name
     * @param Client $client
     */
    public function __construct(
        $id, $name, $code, $type, $isActive, ?Collection $assignedUsers = null, ?array $deploy = null,
        Client $client = null, $createdAt = null, $updatedAt = null
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->id = $id;
        $this->name = $name;
        $this->code = $code;
        $this->type = $type;
        $this->client = $client;
        $this->assignedUsers = $assignedUsers;
        $this->deploy = $deploy;
        $this->isActive = $isActive;
    }

    /**
     * @return Deploy|null
     */
    public function getDeploy()
    {
        return $this->deploy;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function isUserAssigned($user)
    {
        foreach ($this->assignedUsers as $assigned) {
            if ($assigned->getId() == $user->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array|mixed
     */
    public function getAssignedUsers(): mixed
    {
        return $this->assignedUsers;
    }

    public function getHrTypes()
    {
        return [
            static::TYPE_IN => 'In',
            static::TYPE_OUT => 'Out'
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

}