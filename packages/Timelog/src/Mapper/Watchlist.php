<?php
namespace Gfinance\Timelog\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;

class Watchlist extends MysqlCrudMapper
{
    public function __construct(\PDO $pdo)
    {
        parent::__construct($pdo, 'watchlist');
    }
}