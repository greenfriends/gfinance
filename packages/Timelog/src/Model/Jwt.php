<?php

namespace Gfinance\Timelog\Model;

use Skeletor\Model\Model;

class Jwt extends Model
{
    private $jwtId;

    private $jwt;

    private $expiresAt;

    private $userId;

    /**
     * Jwt constructor.
     * @param $jwtId
     * @param $jwt
     * @param $expiresAt
     * @param $userId
     */
    public function __construct($jwtId, $jwt, $expiresAt, $userId, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->jwtId = $jwtId;
        $this->jwt = $jwt;
        $this->expiresAt = $expiresAt;
        $this->userId = $userId;
    }

    public function getId()
    {
        return $this->jwtId;
    }

    /**
     * @return mixed
     */
    public function getJwtId()
    {
        return $this->jwtId;
    }

    /**
     * @return mixed
     */
    public function getJwt()
    {
        return $this->jwt;
    }

    /**
     * @return mixed
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }
}