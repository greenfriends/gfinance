<?php

namespace Gfinance\Timelog\Model;

use Gfinance\Project\Model\Project;
use Skeletor\Model\Model;
use Skeletor\User\Model\User;

class Watchlist extends Model
{
    private $watchlistId;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var User
     */
    private $user;

    /**
     * Watchlist constructor.
     * @param $watchlistId
     * @param $projectId
     * @param $userId
     */
    public function __construct($watchlistId, Project $project, $user, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->watchlistId = $watchlistId;
        $this->project = $project;
        $this->user = $user;
    }


    /**
     * @return mixed
     */
    public function getWatchlistId()
    {
        return $this->watchlistId;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->getWatchlistId();
    }

}