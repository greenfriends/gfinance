<?php
declare(strict_types=1);
namespace Gfinance\Timelog\Controller;

use Gfinance\Client\Repository\ClientRepository;
use Gfinance\Timelog\Service\MailChecker;
use GuzzleHttp\Psr7\Response;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Controller\Controller;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;


class TicketingController extends Controller
{
    /**
     * @var ClientRepository
     */
    private $clientRepo;

    /**
     * @var User
     */
    private $userService;

    private $logger;

    private $mailChecker;

    /**
     * ArticleController constructor.
     * @param Article $articleService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $userService
     */
    public function __construct(
        ClientRepository $clientRepo, Session $session, Config $config, Flash $flash, \Twig\Environment $twig,
        User $userService, Logger $logger, MailChecker $mailChecker
    ) {
        parent::__construct($twig, $config, $session, $flash);

        $this->clientRepo = $clientRepo;
        $this->userService = $userService;
        $this->logger = $logger;
        $this->mailChecker = $mailChecker;
    }

    public function checkMail(): Response
    {
        $this->mailChecker->checkMail();
        die('done');

        return $this->getResponse();
    }

    public function create(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        try {
            $this->clientRepo->create($data);
            $this->getFlash()->success('Client successfully created');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
        return $this->redirect('/client/view/');
    }

    public function update(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        //@TODO validate data
        try {
            $this->clientRepo->update($data);
            $this->getFlash()->success('Client created.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect('/client/view/');
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('clientId');
        $client = false;
        $this->setGlobalVariable('pageTitle', 'Create client');
        if ($id) {
            $client = $this->clientRepo->getById($id);
            $this->setGlobalVariable('pageTitle', 'Edit client: ' . $client->getName());
        }
        $users = $this->userService->getRepository()->fetchAll();

        return $this->respondPartial('form', [
            'client' => $client,
            'users' => $users
        ]);
    }

    /**
     * Articles list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View clients');

        return $this->respond('view', ['clients' => $this->clientRepo->fetchAll()]);
    }

    public function delete(): Response
    {
        try {
            $this->clientRepo->deleteClient((int) $this->getRequest()->getAttribute('clientId'));
            $this->getFlash()->success('Client successfully deleted');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/client/view/');
    }

    public function ajaxHandle(): Response
    {
        $data = $this->getRequest()->getParsedBody();
        $dtRequest = new DataTablesRequest($data);
        $response = $this->getResponse()->withAddedHeader('Content-Type', 'application/json');
        $response->getBody()->write($this->articleService->getDataTableResponse($dtRequest)->getData());

        return $response;
    }

}