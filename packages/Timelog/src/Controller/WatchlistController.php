<?php
declare(strict_types=1);
namespace Gfinance\Timelog\Controller;

use GuzzleHttp\Psr7\Response;
use Gfinance\Timelog\Repository\WatchlistRepository;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Skeletor\User\Service\User;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\Controller;
use Psr\Log\LoggerInterface as Logger;


class WatchlistController extends Controller
{
    /**
     * @var WatchlistRepository
     */
    private $watchlistRepo;

    /**
     * @var User
     */
    private $userService;

    private $logger;

    /**
     * ArticleController constructor.
     * @param Article $articleService
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param \Twig\Environment $twig
     * @param User $userService
     */
    public function __construct(
        WatchlistRepository $watchlistRepo, Session $session, Config $config, Flash $flash, \Twig\Environment $twig,
        User $userService, Logger $logger
    ) {
        parent::__construct($twig, $config, $session, $flash);

        $this->watchlistRepo = $watchlistRepo;
        $this->userService = $userService;
        $this->logger = $logger;
    }

    public function index(): Response
    {
        $this->setGlobalVariable('pageTitle', 'View watchlist');

        return $this->respond('view', [
            'watchlist' => $this->watchlistRepo->fetchAll(['userId' => $this->getLoggedInUserId()]),
        ]);
    }

    public function create(): Response
    {
        try {
            $this->watchlistRepo->create([
                'userId' => $this->getLoggedInUserId(),
                'projectId' => $this->getRequest()->getAttribute('watchlistId') // used attribute here for convenience
            ]);
            $this->getFlash()->success('You are now subscribed to updates for this project.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
        return $this->redirect('/project/view/');
    }

    public function form(): Response
    {
//        $id = (int) $this->getRequest()->getAttribute('watchlistId');
//        $watchlist = false;
//        $this->setGlobalVariable('pageTitle', 'Create watchlist');

    }

    public function unwatch(): Response
    {
        try {
            $item = $this->watchlistRepo->fetchAll(['userId' => $this->getLoggedInUserId(), 'projectId' => $this->getRequest()->getAttribute('watchlistId')]);
            $this->watchlistRepo->delete($item[0]->getId());
            $this->getFlash()->success('No longer subscribed to this project.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/watchlist/index/');
    }

    public function delete(): Response
    {
        try {
            $this->watchlistRepo->delete((int) $this->getRequest()->getAttribute('watchlistId'));
            $this->getFlash()->success('No longer subscribed to this project.');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        return $this->redirect('/watchlist/index/');
    }

}