<?php
declare(strict_types=1);

namespace Gfinance\Timelog\Repository;

use Skeletor\Mapper\NotFoundException;
use Gfinance\Timelog\Mapper\Jwt as Mapper;
use Gfinance\Timelog\Model\Jwt as Model;
use Skeletor\User\Service\User;
use Laminas\Config\Config;


class JwtRepository
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $mapper, User $userService, \DateTime $dt, Config $config)
    {
        $this->mapper = $mapper;
        $this->userService = $userService;
        $this->dt = $dt;
        $this->config = $config;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $userId
     * @return User
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data): Model
    {
        $this->mapper->update($data);

        return $this->getById($data['jwtId']);
    }

    public function create($data): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    private function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
            $data['jwtId'],
            $data['jwt'],
            $data['expiresAt'],
            $data['userId'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    /**
     * Deletes a single user entity.
     * @TODO cleanup tasks, logs and whatnot
     * @param int $clientId
     *
     * @return bool
     */
    public function delete($id): bool
    {
        return $this->mapper->delete($id);
    }
}
