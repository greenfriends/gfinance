<?php
declare(strict_types=1);

namespace Gfinance\Timelog\Repository;

use Gfinance\Project\Repository\ProjectRepository;
use Gfinance\Timelog\Mapper\Watchlist as Mapper;
use Gfinance\Timelog\Model\Watchlist as Model;
use Gfinance\Timelog\Service\User;
use Skeletor\Mapper\NotFoundException;

class WatchlistRepository
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var ProjectRepository
     */
    private $projectRepo;

    private $userService;

    /**
     * userRepository constructor.
     *
     * @param Mapper $userMapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $watchlistMapper, \DateTime $dt, ProjectRepository $projectRepo, User $userService)
    {
        $this->mapper = $watchlistMapper;
        $this->dt = $dt;
        $this->projectRepo = $projectRepo;
        $this->userService = $userService;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array()): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $userId
     * @return User
     * @throws NotFoundException
     */
    public function getById($watchlistId): Model
    {
        return $this->make($this->mapper->fetchById((int) $watchlistId));
    }

    /**
     * Updates user model.
     *
     * @param $data
     * @return User
     * @throws \Exception
     *
     */
    public function update($data): Model
    {
        $this->mapper->update($data);

        return $this->getById($data['watchlistId']);
    }

    public function create($data): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    private function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }
        $project = $this->projectRepo->getById($data['projectId']);
        $user = $this->userService->getRepository()->getById($data['userId']);

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
            $data['watchlistId'],
            $project,
            $user,
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function getWatchlist($userId)
    {
        $projects = [];
        foreach ($this->fetchAll(['userId' => $userId]) as $item) {
            $projects[] = $item->getProject()->getId();
        }

        return $projects;
    }

    public function delete($watchlistId): bool
    {
        return $this->mapper->delete($watchlistId);
    }
}
