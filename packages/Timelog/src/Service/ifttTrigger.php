<?php
/**
 * Created by PhpStorm.
 * User: djavolak
 * Date: 10/10/2021
 * Time: 7:12 PM
 */

namespace Gfinance\Timelog\Service;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ifttTrigger
{
    private $httpClient;

    /**
     * ifttTrigger constructor.
     * @param $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function customTrigger($triggerName, $data)
    {
        $url = sprintf(
            'https://maker.ifttt.com/trigger/%s/json/with/key/nFm5NXYy-aj7t5I84P8LMHLJZ0T0aCRu4JLtZgCxi3S',
            $triggerName
        );

        $this->sendTrigger($url, $data);
    }

    public function newTask(\Gfinance\Timelog\Model\Task $task)
    {
        $url = sprintf(
            'https://maker.ifttt.com/trigger/%sTaskAdded/json/with/key/nFm5NXYy-aj7t5I84P8LMHLJZ0T0aCRu4JLtZgCxi3S',
            $task->getProject()->getCode()
        );
        $data = [
            'taskUrl' => sprintf('http://jira.djavolak.info/task/form/%d/', $task->getId()),
            'title' => $task->getName(),
        ];

        $this->sendTrigger($url, $data);
    }

    private function sendTrigger($url, $data)
    {
        $request = new Request('POST', $url, ['Content-Type' => 'application/json'], json_encode($data));
        try {
            $response = $this->httpClient->send($request);
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}