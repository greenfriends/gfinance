<?php
declare(strict_types=1);
namespace Gfinance\Timelog\Service;

use \Firebase\JWT\JWT as JwtToken;
use Gfinance\Timelog\Repository\JwtRepository;

class Jwt
{
    const SECRET_KEY = 'someSecretKey';

    private $jwtRepo;

    private $user;

    /**
     * Jwt constructor.
     * @param $jwtRepo
     */
    public function __construct(JwtRepository $jwtRepo, \Skeletor\User\Service\User $user)
    {
        $this->jwtRepo = $jwtRepo;
        $this->user = $user;
    }

    public function issueAccessToken(\Skeletor\User\Model\User $user)
    {
        $message = 'Access token.';
        $dt = new \DateTime('now');
        $issuedAt = $dt->getTimestamp();
        $dt->modify('+4 hour');
        $accessExpiry = $dt->getTimestamp();

        return json_encode([
            "message" => $message,
            "jwt" => $this->compileJwt($user, $accessExpiry, $issuedAt),
//            "refreshToken" => $this->compileJwt($user, $refreshExpiry, $issuedAt),
            "email" => $user->getEmail(),
            "expireAt" => $accessExpiry
        ]);
    }

    public function issueRefreshToken(\Skeletor\User\Model\User $user)
    {
        $dt = new \DateTime('now');
        $issuedAt = $dt->getTimestamp();
        $dt->modify('+7 day');
        $expiresAt = $dt->getTimestamp();
        $message = 'Refresh token.';
        $token = $this->compileJwt($user, $expiresAt, $issuedAt);
        $this->jwtRepo->create([
            'jwt' => $token,
            'expiresAt' => $dt->format('Y-m-d H:i:s'),
            'userId' => $user->getId(),
        ]);

        return json_encode([
            "message" => $message,
            "jwt" => $this->compileJwt($user, $expiresAt, $issuedAt),
            "email" => $user->getEmail(),
            "expireAt" => $expiresAt
        ]);
    }

    public function refreshAccessToken($refreshToken)
    {
        // expiresAt > NOW
        $jwt = $this->jwtRepo->fetchAll(['jwt' => $refreshToken])[0];
        if (!$jwt) {
            throw new \Exception('Token not found');
        }
        $tokenData = JwtToken::decode($jwt->getJwt(), self::SECRET_KEY, ['HS256']);
        $user = $this->user->getRepository()->getById($tokenData->data->id);

        return $this->issueAccessToken($user);
    }

    private function compileJwt(\Skeletor\User\Model\User $user, $expiresAt, $issuedAt)
    {
        $issuer_claim = "gfinance";
        $audience_claim = "THE_AUDIENCE";
        $issuedat_claim = $issuedAt;
        $notbefore_claim = $issuedat_claim;
        $expire_claim = $expiresAt;
        $token = array(
            "iss" => $issuer_claim,
            "aud" => $audience_claim,
            "iat" => $issuedat_claim,
            "nbf" => $notbefore_claim,
            "exp" => $expire_claim,
            "data" => array(
                "id" => $user->getId(),
                "email" => $user->getEmail(),
            ));

        return JwtToken::encode($token, self::SECRET_KEY);
    }

    private function compileToken(\Skeletor\User\Model\User $user, $expiresAt, $issuedAt, $message)
    {


        return json_encode([
            "message" => $message,
            "jwt" => $jwt,
            "email" => $user->getEmail(),
            "expireAt" => $expire_claim
        ]);
    }
}