<?php

namespace Gfinance\Timelog\Service;

use Gfinance\Task\Repository\TaskRepository;
use Gfinance\Timelog\Model\Task;
use Gfinance\Timelog\Model\Task\Status;
use Gfinance\Timelog\Model\Watchlist;
use Gfinance\Timelog\Repository\WatchlistRepository;
use Twig\Environment;

class Notification
{

    private $taskRepository;

    private $config;

    private $template;

    private $watchlistRepo;

    public function __construct(TaskRepository $taskRepository, Mailer $mailer, Environment $template, WatchlistRepository $watchlistRepo)
    {
        $this->taskRepository = $taskRepository;
        $this->template = $template;
        $this->mailer = $mailer;
        $this->watchlistRepo = $watchlistRepo;
    }

    public function sendTicketNotice(Task $task, $type = 'update')
    {
        if ($type === 'create') {
            $this->sendNewTicketNotice($task);
            return;
        }
        $this->sendTicketUpdateNotice($task);
    }

    private function sendNewTicketNotice(Task $task)
    {
        /* @var Watchlist $item*/
        foreach ($this->watchlistRepo->fetchAll(['projectId' => $task->getProject()->getId()]) as $item) {
            $body = $this->template->render('email/newTicket.twig', [
                'url' => 'http://jira.djavolak.info/task/form/' .$task->getId(). '/',
                'project' => $item->getProject()->getName(),
                'name' => $item->getUser()->getDisplayName(),
                'title' => $task->getName(),
            ]);
            $subject = 'New ticket received for ' . $item->getProject()->getName();
            $this->mailer->sendTicketNotice($subject, $item->getUser()->getEmail(), $body);
        }
    }

    private function sendTicketUpdateNotice(Task $task)
    {
        /* @var Watchlist $item*/
        foreach ($this->watchlistRepo->fetchAll(['projectId' => $task->getProject()->getId()]) as $item) {
            $body = $this->template->render('email/updateTicket.twig', [
                'url' => 'http://jira.djavolak.info/task/form/' .$task->getId(). '/',
                'project' => $item->getProject()->getName(),
                'ticket' => $task->getCode(),
                'name' => $item->getUser()->getDisplayName(),
            ]);
            $subject = sprintf('Ticket %s in %s is updated.', $task->getCode(), $item->getProject()->getName());
            $this->mailer->sendTicketNotice($subject, $item->getUser()->getEmail(), $body);
        }
    }

}