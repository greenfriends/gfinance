<?php

namespace Gfinance\Timelog\Service;

use Laminas\Mail\Transport\TransportInterface;
use Laminas\Mail\Message;
use \Laminas\Config\Config;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Part;
use Laminas\Mime\Mime;

class Mailer
{
    /**
     * @var TransportInterface
     */
    private $mail;

    private $baseUrl;

    public function __construct(TransportInterface $mail, Config $config)
    {
        $this->mail = $mail;
        $this->baseUrl = $config->baseUrl;
    }

    public function sendTicketNotice($subject, $recipientEmail, $body)
    {
        $mimeMessage = new MimeMessage();
        $html = new Part();
        $html->type = Mime::TYPE_HTML;
        $html->charset = 'utf-8';
        $html->setContent($body);
        $mimeMessage->addPart($html);
        $message = new Message();
        $message->addTo($recipientEmail)
            ->setFrom('no-reply@greenfriends.systems')
            ->setSubject($subject)
            ->setBody($mimeMessage);
        $this->send($message);
    }

    public function applicationErrorNotice(array $record)
    {
        $message = new Message();
        $message
            ->addTo('djavolak@mail.ru')
            ->setFrom('mailer@dev')
            ->setSubject('Gfin Application error')
            ->setBody($record['message']);
        $this->send($message);
    }

    private function send(Message $message)
    {
        try {
            $this->mail->send($message);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}