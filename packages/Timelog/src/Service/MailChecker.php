<?php

namespace Gfinance\Timelog\Service;

use Gfinance\Task\Service\Task;
use Gfinance\Timelog\Model\Task\Status;
use Laminas\Config\Config;
use Laminas\Mail\Storage;
use Laminas\Mail\Storage\Folder;
use Laminas\Mail\Storage\Imap;
use Laminas\Mail\Storage\Message;

//use Gfinance\Timelog\Repository\TaskRepository;

class MailChecker
{

    private $foldersToCheck = [
        13 => 'ticketing-tip',
        2 => 'ticketing-dijaspora',
        6 => 'ticketing-mb',
        7 => 'ticketing-nss',
        8 => 'ticketing-pilates',
        10 => 'ticketing-si',
        12 => 'ticketing-inmemo',
    ];

    private $taskService;

    private $config;

    public function __construct(Task $taskService, Config $config)
    {
        $this->taskService = $taskService;
        $this->config = [
            'host'     => $config->mailServer->host,
            'user'     => $config->mailServer->connection_config->username,
            'password' => $config->mailServer->connection_config->password,
            'ssl' => 'ssl',
        ];
    }

    public function checkMail()
    {
        $mail = new Imap($this->config);
        /* @var Folder $folder */
        foreach ($mail->getFolders() as $folder) {
            if (in_array($folder->getLocalName(), $this->foldersToCheck)) {
                $projectId = array_search($folder->getLocalName(), $this->foldersToCheck);
                $mail->selectFolder($folder);
                $this->checkFolderForMail($mail, $projectId);
            }
        }
    }

    private function checkFolderForMail(Imap $mail, $projectId)
    {
        foreach ($mail as $id => $message) {
            if (!$message->hasFlag(Storage::FLAG_SEEN)) {
                $this->createTask($message, $projectId);
                //@TODO send auto reply
//                $mail->setFlags($id, [Storage::FLAG_SEEN]);
            } else {
//                var_dump($message->getFlags());
//                die();
            }
        }
    }

    private function createTask(Message $message, $projectId)
    {
        echo 'creating task...' . PHP_EOL;

        $breakStrings = [
            '-----Original Message-----',
            '<html>',
            'From: ',
            'Sent from my Iphone',
            'Sent from my BlackBerry',
            'Svako dobro,' . PHP_EOL,
            'ozdrav,',
            'ozdrav,',
            'Srdačno,',
            PHP_EOL
        ];

//        var_dump($message->isMultipart());
//        var_dump($message->countParts());
        $body = '<p>Ticket sent by ' . $message->from . '</p>';
        $files = [];
        for ($i=1; $i<=$message->countParts(); $i++) {
            echo "part {$i}" . PHP_EOL;
            $content = $message->getPart($i)->getContent() . PHP_EOL;
            $encoded = false;
            try {
                if ($message->getPart($i)->getHeader('Content-Transfer-Encoding')->toString() === 'Content-Transfer-Encoding: base64') {
                    $encoded = true;
                    $content = base64_decode($message->getPart($i)->getContent()) . PHP_EOL;
                }
            } catch (\Exception $e) {

            }

            if ($encoded) {
                $regex = '/name="([^"]+)"/';
                preg_match($regex, $message->getPart($i)->contentType, $matches, PREG_OFFSET_CAPTURE, 0);
                if (count($matches)) {
                    $files[] = [
                        'fileName' => $matches[1][0],
                        'fileData' => $content
                    ];
                }
            } else {
                foreach (explode(PHP_EOL, $content) as $line) {
                    if (strlen(trim($line)) === 0) {
                        continue;
                    }
                    if ($line === '-- \n' || $line === '--\n') {
                        break;
                    }
                    if (false !== strpos($line, '-----') || false !== strpos($line, '______')) {
                        break;
                    }
                    foreach ($breakStrings as $string) {
                        if (false !== strpos($line, $string)) {
                            break 2;
                        }
                    }
//                var_dump($line);
                    $body .= '<p>' . $line . '</p>';
                }
            }
        }

        $task = $this->taskService->create([
            'name' => $message->subject,
            'description' => $body,
            'status' => Status::STATUS_NEW,
            'projectId' => $projectId,
            'assignedUsers' => [],
            'createdBy' => 8,
        ], []);

        foreach ($files as $file) {
            $this->taskService->saveAttachmentFromTicket($file['fileData'], $file['fileName'], $task->getId());
        }
    }

}